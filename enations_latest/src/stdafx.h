//---------------------------------------------------------------------------
//
//	Copyright (c) 1995, 1996. Windward Studios, Inc.
//	All Rights Reserved.
//
//---------------------------------------------------------------------------


#ifndef __STDAFX_H__
#define __STDAFX_H__

#pragma warning ( disable : 4711 )


// MFC libs - The dependency on these needs to be eliminated if game is to be made cross platform
#include <afxwin.h>			// MFC core and standard components
#include <afxext.h>
#include <afxcmn.h>
#include <afxtempl.h>
#include <afxmt.h>
#include <mmsystem.h>
#include <mmreg.h>
#include <msacm.h>
#include <stdio.h>

// C++ std headers - Some of these have been updated to use the C++ standard libraries.
#include <climits>
#include <malloc.h> // C memory management - Should probably be removed since RAM is plentiful.
#include <cmath>
#include <strstream> // strstream - Changed from strstrea.h
//#include <ctl3d.h> // Old 3D effect library - Has been removed at some point
#include <eh.h>
#include <ctype.h>
#include <locale.h>

// Direct X SDK headers - The dependency on these also should to be elimated and replaced with OpenGL/Mantle/Vulkan (Or at least a more up to date graphics API).
#include <ddraw.h>
#include <dsound.h>
//#include <dplay.h>

#include <wing.h> // WinGee GDI - Predecessor to ddraw to help port games from DOS

#include <mssw.h> // Miles Sound System - An old 16-bit version which should be removed

// Smart Heap - Alternitive heap managment. 
// More or less preventing game from compiling on modern OSes.
// Needs to be removed from source code.
//#define MEM_DEBUG	1
//#include <smrtheap.hpp>

#include "windward.h"
#include "vdmplay.h"

#pragma warning ( disable : 4244 )	// I don't like this!!!

#ifdef	_DEBUG
#define		_CHEAT	1
#endif


#endif
