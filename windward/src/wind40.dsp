# Microsoft Developer Studio Project File - Name="Windward" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=Windward - Win32 Release
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "wind40.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "wind40.mak" CFG="Windward - Win32 Release"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Windward - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "Windward - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "Windward - Win32 Release_dll" (based on "Win32 (x86) Static Library")
!MESSAGE "Windward - Win32 Debug_dll" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Windward - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir ".\WinRel"
# PROP BASE Intermediate_Dir ".\WinRel"
# PROP Use_MFC 1
# PROP Use_Debug_Libraries 0
# PROP Output_Dir ".\Release"
# PROP Intermediate_Dir ".\Release"
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /FR /YX /c
# ADD CPP /nologo /MT /W3 /GX /Zi /O2 /Ob2 /I "..\include" /I "c:\src\include" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /FR /YX /FD /c
# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:".\Release\wind.lib"

!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir ".\WinDebug"
# PROP BASE Intermediate_Dir ".\WinDebug"
# PROP Use_MFC 1
# PROP Use_Debug_Libraries 1
# PROP Output_Dir ".\Debug"
# PROP Intermediate_Dir ".\Debug"
# ADD BASE CPP /nologo /W3 /GX /Z7 /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FR /YX /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I "..\include" /I "c:\src\include" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_CHEAT" /D "_MBCS" /FR /YX /FD /c
# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:".\debug\Windd.lib"

!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"

# PROP BASE Use_MFC 1
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir ".\Windward"
# PROP BASE Intermediate_Dir ".\Windward"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir ".\ReleaseDLL"
# PROP Intermediate_Dir ".\ReleaseDLL"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /Zi /O2 /Ob2 /I "..\include" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /FR /YX /c
# ADD CPP /nologo /MD /W3 /GX /Zi /O2 /Ob2 /I "..\include" /I "c:\src\include" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /YX /FD /c
# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:".\ReleaseDLL\windDLL.lib"

!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"

# PROP BASE Use_MFC 1
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir ".\Windwar0"
# PROP BASE Intermediate_Dir ".\Windwar0"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir ".\DebugDLL"
# PROP Intermediate_Dir ".\DebugDLL"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /Zi /Od /I "..\include" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_CHEAT" /D "_MBCS" /FR /YX /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "..\include" /I "c:\src\include" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_CHEAT" /D "_AFXDLL" /D "_MBCS" /FR /YX /FD /c
# ADD BASE RSC /l 0x409
# ADD RSC /l 0x409
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"WinDeb40\Wind40d.lib"
# ADD LIB32 /nologo /out:".\debugdll\WindDLLd.lib"

!ENDIF 

# Begin Target

# Name "Windward - Win32 Release"
# Name "Windward - Win32 Debug"
# Name "Windward - Win32 Release_dll"
# Name "Windward - Win32 Debug_dll"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat;for;f90"
# Begin Source File

SOURCE=.\APPPALET.CPP
# End Source File
# Begin Source File

SOURCE=.\BITBUFFE.CPP
# End Source File
# Begin Source File

SOURCE=.\BLT.CPP
# End Source File
# Begin Source File

SOURCE=.\BPECODEC.CPP
# End Source File
# Begin Source File

SOURCE=.\codec.cpp
# End Source File
# Begin Source File

SOURCE=.\DAVIDINL.CPP
# End Source File
# Begin Source File

SOURCE=.\DIBWND.CPP
# End Source File
# Begin Source File

SOURCE=.\DlgMsg.cpp
# End Source File
# Begin Source File

SOURCE=.\Flcanim.cpp
# End Source File
# Begin Source File

SOURCE=.\Flcctrl.cpp
# End Source File
# Begin Source File

SOURCE=.\GLOBAL.CPP
# End Source File
# Begin Source File

SOURCE=.\HUFFMANC.CPP
# End Source File
# Begin Source File

SOURCE=.\INIT.CPP
# End Source File
# Begin Source File

SOURCE=.\logging.CPP
# End Source File
# Begin Source File

SOURCE=.\LZSSCODE.CPP
# End Source File
# Begin Source File

SOURCE=.\LZWCODEC.CPP
# End Source File
# Begin Source File

SOURCE=.\MSG_BOX.CPP
# End Source File
# Begin Source File

SOURCE=.\RAND.CPP
# End Source File
# Begin Source File

SOURCE=.\STDAFX.CPP
# End Source File
# Begin Source File

SOURCE=.\STREXT.CPP
# End Source File
# Begin Source File

SOURCE=.\THREADS.CPP
# End Source File
# Begin Source File

SOURCE=.\WINDWARD.RC
# End Source File
# Begin Source File

SOURCE=.\wndbase.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl;fi;fd"
# Begin Source File

SOURCE=.\STDAFX.H
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;cnt;rtf;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
