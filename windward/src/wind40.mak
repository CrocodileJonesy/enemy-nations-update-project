# Microsoft Developer Studio Generated NMAKE File, Format Version 4.20
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

!IF "$(CFG)" == ""
CFG=Windward - Win32 Debug_dll
!MESSAGE No configuration specified.  Defaulting to Windward - Win32 Debug_dll.
!ENDIF 

!IF "$(CFG)" != "Windward - Win32 Release" && "$(CFG)" !=\
 "Windward - Win32 Debug" && "$(CFG)" != "Windward - Win32 Release_dll" &&\
 "$(CFG)" != "Windward - Win32 Debug_dll"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE on this makefile
!MESSAGE by defining the macro CFG on the command line.  For example:
!MESSAGE 
!MESSAGE NMAKE /f "wind40.mak" CFG="Windward - Win32 Debug_dll"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Windward - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "Windward - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "Windward - Win32 Release_dll" (based on "Win32 (x86) Static Library")
!MESSAGE "Windward - Win32 Debug_dll" (based on "Win32 (x86) Static Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 
################################################################################
# Begin Project
# PROP Target_Last_Scanned "Windward - Win32 Debug_dll"
CPP=cl.exe

!IF  "$(CFG)" == "Windward - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "WinRel"
# PROP BASE Intermediate_Dir "WinRel"
# PROP Use_MFC 1
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
OUTDIR=.\Release
INTDIR=.\Release

ALL : "$(OUTDIR)\wind.lib" "$(OUTDIR)\wind40.bsc"

CLEAN : 
	-@erase "$(INTDIR)\ACMUTIL.OBJ"
	-@erase "$(INTDIR)\ACMUTIL.SBR"
	-@erase "$(INTDIR)\APPPALET.OBJ"
	-@erase "$(INTDIR)\APPPALET.SBR"
	-@erase "$(INTDIR)\BITBUFFE.OBJ"
	-@erase "$(INTDIR)\BITBUFFE.SBR"
	-@erase "$(INTDIR)\BLT.OBJ"
	-@erase "$(INTDIR)\BLT.SBR"
	-@erase "$(INTDIR)\BPECODEC.OBJ"
	-@erase "$(INTDIR)\BPECODEC.SBR"
	-@erase "$(INTDIR)\BTREE.OBJ"
	-@erase "$(INTDIR)\BTREE.SBR"
	-@erase "$(INTDIR)\cache.obj"
	-@erase "$(INTDIR)\cache.sbr"
	-@erase "$(INTDIR)\codec.obj"
	-@erase "$(INTDIR)\codec.sbr"
	-@erase "$(INTDIR)\DATAFILE.OBJ"
	-@erase "$(INTDIR)\DATAFILE.SBR"
	-@erase "$(INTDIR)\DAVIDINL.OBJ"
	-@erase "$(INTDIR)\DAVIDINL.SBR"
	-@erase "$(INTDIR)\DIB.OBJ"
	-@erase "$(INTDIR)\DIB.SBR"
	-@erase "$(INTDIR)\DIBWND.OBJ"
	-@erase "$(INTDIR)\DIBWND.SBR"
	-@erase "$(INTDIR)\DlgMsg.obj"
	-@erase "$(INTDIR)\DlgMsg.sbr"
	-@erase "$(INTDIR)\FIXPOINT.OBJ"
	-@erase "$(INTDIR)\FIXPOINT.SBR"
	-@erase "$(INTDIR)\Flcanim.obj"
	-@erase "$(INTDIR)\Flcanim.sbr"
	-@erase "$(INTDIR)\Flcctrl.obj"
	-@erase "$(INTDIR)\Flcctrl.sbr"
	-@erase "$(INTDIR)\GLOBAL.OBJ"
	-@erase "$(INTDIR)\GLOBAL.SBR"
	-@erase "$(INTDIR)\HUFFMANC.OBJ"
	-@erase "$(INTDIR)\HUFFMANC.SBR"
	-@erase "$(INTDIR)\INIT.OBJ"
	-@erase "$(INTDIR)\INIT.SBR"
	-@erase "$(INTDIR)\logging.obj"
	-@erase "$(INTDIR)\logging.sbr"
	-@erase "$(INTDIR)\LZSSCODE.OBJ"
	-@erase "$(INTDIR)\LZSSCODE.SBR"
	-@erase "$(INTDIR)\LZWCODEC.OBJ"
	-@erase "$(INTDIR)\LZWCODEC.SBR"
	-@erase "$(INTDIR)\MMIO.OBJ"
	-@erase "$(INTDIR)\MMIO.SBR"
	-@erase "$(INTDIR)\MSG_BOX.OBJ"
	-@erase "$(INTDIR)\MSG_BOX.SBR"
	-@erase "$(INTDIR)\MUSIC.OBJ"
	-@erase "$(INTDIR)\MUSIC.SBR"
	-@erase "$(INTDIR)\RAND.OBJ"
	-@erase "$(INTDIR)\RAND.SBR"
	-@erase "$(INTDIR)\SCANLIST.OBJ"
	-@erase "$(INTDIR)\SCANLIST.SBR"
	-@erase "$(INTDIR)\STDAFX.OBJ"
	-@erase "$(INTDIR)\STDAFX.SBR"
	-@erase "$(INTDIR)\STREXT.OBJ"
	-@erase "$(INTDIR)\STREXT.SBR"
	-@erase "$(INTDIR)\Subclass.obj"
	-@erase "$(INTDIR)\Subclass.sbr"
	-@erase "$(INTDIR)\THREADS.OBJ"
	-@erase "$(INTDIR)\THREADS.SBR"
	-@erase "$(INTDIR)\vc40.pdb"
	-@erase "$(INTDIR)\wndbase.obj"
	-@erase "$(INTDIR)\wndbase.sbr"
	-@erase "$(OUTDIR)\wind.lib"
	-@erase "$(OUTDIR)\wind40.bsc"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /FR /YX /c
# ADD CPP /nologo /MT /W3 /GX /Zi /O2 /Ob2 /I "..\include" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /FR /YX /c
CPP_PROJ=/nologo /MT /W3 /GX /Zi /O2 /Ob2 /I "..\include" /D "NDEBUG" /D\
 "WIN32" /D "_WINDOWS" /D "_MBCS" /FR"$(INTDIR)/" /Fp"$(INTDIR)/wind40.pch" /YX\
 /Fo"$(INTDIR)/" /Fd"$(INTDIR)/" /c 
CPP_OBJS=.\Release/
CPP_SBRS=.\Release/
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/wind40.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\ACMUTIL.SBR" \
	"$(INTDIR)\APPPALET.SBR" \
	"$(INTDIR)\BITBUFFE.SBR" \
	"$(INTDIR)\BLT.SBR" \
	"$(INTDIR)\BPECODEC.SBR" \
	"$(INTDIR)\BTREE.SBR" \
	"$(INTDIR)\cache.sbr" \
	"$(INTDIR)\codec.sbr" \
	"$(INTDIR)\DATAFILE.SBR" \
	"$(INTDIR)\DAVIDINL.SBR" \
	"$(INTDIR)\DIB.SBR" \
	"$(INTDIR)\DIBWND.SBR" \
	"$(INTDIR)\DlgMsg.sbr" \
	"$(INTDIR)\FIXPOINT.SBR" \
	"$(INTDIR)\Flcanim.sbr" \
	"$(INTDIR)\Flcctrl.sbr" \
	"$(INTDIR)\GLOBAL.SBR" \
	"$(INTDIR)\HUFFMANC.SBR" \
	"$(INTDIR)\INIT.SBR" \
	"$(INTDIR)\logging.sbr" \
	"$(INTDIR)\LZSSCODE.SBR" \
	"$(INTDIR)\LZWCODEC.SBR" \
	"$(INTDIR)\MMIO.SBR" \
	"$(INTDIR)\MSG_BOX.SBR" \
	"$(INTDIR)\MUSIC.SBR" \
	"$(INTDIR)\RAND.SBR" \
	"$(INTDIR)\SCANLIST.SBR" \
	"$(INTDIR)\STDAFX.SBR" \
	"$(INTDIR)\STREXT.SBR" \
	"$(INTDIR)\Subclass.sbr" \
	"$(INTDIR)\THREADS.SBR" \
	"$(INTDIR)\wndbase.sbr"

"$(OUTDIR)\wind40.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"Release\wind.lib"
LIB32_FLAGS=/nologo /out:"$(OUTDIR)/wind.lib" 
LIB32_OBJS= \
	"$(INTDIR)\ACMUTIL.OBJ" \
	"$(INTDIR)\APPPALET.OBJ" \
	"$(INTDIR)\BITBUFFE.OBJ" \
	"$(INTDIR)\BLT.OBJ" \
	"$(INTDIR)\BPECODEC.OBJ" \
	"$(INTDIR)\BTREE.OBJ" \
	"$(INTDIR)\cache.obj" \
	"$(INTDIR)\codec.obj" \
	"$(INTDIR)\DATAFILE.OBJ" \
	"$(INTDIR)\DAVIDINL.OBJ" \
	"$(INTDIR)\DIB.OBJ" \
	"$(INTDIR)\DIBWND.OBJ" \
	"$(INTDIR)\DlgMsg.obj" \
	"$(INTDIR)\FIXPOINT.OBJ" \
	"$(INTDIR)\Flcanim.obj" \
	"$(INTDIR)\Flcctrl.obj" \
	"$(INTDIR)\GLOBAL.OBJ" \
	"$(INTDIR)\HUFFMANC.OBJ" \
	"$(INTDIR)\INIT.OBJ" \
	"$(INTDIR)\logging.obj" \
	"$(INTDIR)\LZSSCODE.OBJ" \
	"$(INTDIR)\LZWCODEC.OBJ" \
	"$(INTDIR)\MMIO.OBJ" \
	"$(INTDIR)\MSG_BOX.OBJ" \
	"$(INTDIR)\MUSIC.OBJ" \
	"$(INTDIR)\RAND.OBJ" \
	"$(INTDIR)\SCANLIST.OBJ" \
	"$(INTDIR)\STDAFX.OBJ" \
	"$(INTDIR)\STREXT.OBJ" \
	"$(INTDIR)\Subclass.obj" \
	"$(INTDIR)\THREADS.OBJ" \
	"$(INTDIR)\wndbase.obj"

"$(OUTDIR)\wind.lib" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "WinDebug"
# PROP BASE Intermediate_Dir "WinDebug"
# PROP Use_MFC 1
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
OUTDIR=.\Debug
INTDIR=.\Debug

ALL : "$(OUTDIR)\Windd.lib" "$(OUTDIR)\wind40.bsc"

CLEAN : 
	-@erase "$(INTDIR)\ACMUTIL.OBJ"
	-@erase "$(INTDIR)\ACMUTIL.SBR"
	-@erase "$(INTDIR)\APPPALET.OBJ"
	-@erase "$(INTDIR)\APPPALET.SBR"
	-@erase "$(INTDIR)\BITBUFFE.OBJ"
	-@erase "$(INTDIR)\BITBUFFE.SBR"
	-@erase "$(INTDIR)\BLT.OBJ"
	-@erase "$(INTDIR)\BLT.SBR"
	-@erase "$(INTDIR)\BPECODEC.OBJ"
	-@erase "$(INTDIR)\BPECODEC.SBR"
	-@erase "$(INTDIR)\BTREE.OBJ"
	-@erase "$(INTDIR)\BTREE.SBR"
	-@erase "$(INTDIR)\cache.obj"
	-@erase "$(INTDIR)\cache.sbr"
	-@erase "$(INTDIR)\codec.obj"
	-@erase "$(INTDIR)\codec.sbr"
	-@erase "$(INTDIR)\DATAFILE.OBJ"
	-@erase "$(INTDIR)\DATAFILE.SBR"
	-@erase "$(INTDIR)\DAVIDINL.OBJ"
	-@erase "$(INTDIR)\DAVIDINL.SBR"
	-@erase "$(INTDIR)\DIB.OBJ"
	-@erase "$(INTDIR)\DIB.SBR"
	-@erase "$(INTDIR)\DIBWND.OBJ"
	-@erase "$(INTDIR)\DIBWND.SBR"
	-@erase "$(INTDIR)\DlgMsg.obj"
	-@erase "$(INTDIR)\DlgMsg.sbr"
	-@erase "$(INTDIR)\FIXPOINT.OBJ"
	-@erase "$(INTDIR)\FIXPOINT.SBR"
	-@erase "$(INTDIR)\Flcanim.obj"
	-@erase "$(INTDIR)\Flcanim.sbr"
	-@erase "$(INTDIR)\Flcctrl.obj"
	-@erase "$(INTDIR)\Flcctrl.sbr"
	-@erase "$(INTDIR)\GLOBAL.OBJ"
	-@erase "$(INTDIR)\GLOBAL.SBR"
	-@erase "$(INTDIR)\HUFFMANC.OBJ"
	-@erase "$(INTDIR)\HUFFMANC.SBR"
	-@erase "$(INTDIR)\INIT.OBJ"
	-@erase "$(INTDIR)\INIT.SBR"
	-@erase "$(INTDIR)\logging.obj"
	-@erase "$(INTDIR)\logging.sbr"
	-@erase "$(INTDIR)\LZSSCODE.OBJ"
	-@erase "$(INTDIR)\LZSSCODE.SBR"
	-@erase "$(INTDIR)\LZWCODEC.OBJ"
	-@erase "$(INTDIR)\LZWCODEC.SBR"
	-@erase "$(INTDIR)\MMIO.OBJ"
	-@erase "$(INTDIR)\MMIO.SBR"
	-@erase "$(INTDIR)\MSG_BOX.OBJ"
	-@erase "$(INTDIR)\MSG_BOX.SBR"
	-@erase "$(INTDIR)\MUSIC.OBJ"
	-@erase "$(INTDIR)\MUSIC.SBR"
	-@erase "$(INTDIR)\RAND.OBJ"
	-@erase "$(INTDIR)\RAND.SBR"
	-@erase "$(INTDIR)\SCANLIST.OBJ"
	-@erase "$(INTDIR)\SCANLIST.SBR"
	-@erase "$(INTDIR)\STDAFX.OBJ"
	-@erase "$(INTDIR)\STDAFX.SBR"
	-@erase "$(INTDIR)\STREXT.OBJ"
	-@erase "$(INTDIR)\STREXT.SBR"
	-@erase "$(INTDIR)\Subclass.obj"
	-@erase "$(INTDIR)\Subclass.sbr"
	-@erase "$(INTDIR)\THREADS.OBJ"
	-@erase "$(INTDIR)\THREADS.SBR"
	-@erase "$(INTDIR)\vc40.idb"
	-@erase "$(INTDIR)\vc40.pdb"
	-@erase "$(INTDIR)\wndbase.obj"
	-@erase "$(INTDIR)\wndbase.sbr"
	-@erase "$(OUTDIR)\wind40.bsc"
	-@erase "$(OUTDIR)\Windd.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE CPP /nologo /W3 /GX /Z7 /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FR /YX /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /Zi /Od /I "..\include" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_CHEAT" /D "_MBCS" /FR /YX /c
CPP_PROJ=/nologo /MTd /W3 /Gm /GX /Zi /Od /I "..\include" /D "_DEBUG" /D\
 "WIN32" /D "_WINDOWS" /D "_CHEAT" /D "_MBCS" /FR"$(INTDIR)/"\
 /Fp"$(INTDIR)/wind40.pch" /YX /Fo"$(INTDIR)/" /Fd"$(INTDIR)/" /c 
CPP_OBJS=.\Debug/
CPP_SBRS=.\Debug/
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/wind40.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\ACMUTIL.SBR" \
	"$(INTDIR)\APPPALET.SBR" \
	"$(INTDIR)\BITBUFFE.SBR" \
	"$(INTDIR)\BLT.SBR" \
	"$(INTDIR)\BPECODEC.SBR" \
	"$(INTDIR)\BTREE.SBR" \
	"$(INTDIR)\cache.sbr" \
	"$(INTDIR)\codec.sbr" \
	"$(INTDIR)\DATAFILE.SBR" \
	"$(INTDIR)\DAVIDINL.SBR" \
	"$(INTDIR)\DIB.SBR" \
	"$(INTDIR)\DIBWND.SBR" \
	"$(INTDIR)\DlgMsg.sbr" \
	"$(INTDIR)\FIXPOINT.SBR" \
	"$(INTDIR)\Flcanim.sbr" \
	"$(INTDIR)\Flcctrl.sbr" \
	"$(INTDIR)\GLOBAL.SBR" \
	"$(INTDIR)\HUFFMANC.SBR" \
	"$(INTDIR)\INIT.SBR" \
	"$(INTDIR)\logging.sbr" \
	"$(INTDIR)\LZSSCODE.SBR" \
	"$(INTDIR)\LZWCODEC.SBR" \
	"$(INTDIR)\MMIO.SBR" \
	"$(INTDIR)\MSG_BOX.SBR" \
	"$(INTDIR)\MUSIC.SBR" \
	"$(INTDIR)\RAND.SBR" \
	"$(INTDIR)\SCANLIST.SBR" \
	"$(INTDIR)\STDAFX.SBR" \
	"$(INTDIR)\STREXT.SBR" \
	"$(INTDIR)\Subclass.sbr" \
	"$(INTDIR)\THREADS.SBR" \
	"$(INTDIR)\wndbase.sbr"

"$(OUTDIR)\wind40.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"debug\Windd.lib"
LIB32_FLAGS=/nologo /out:"$(OUTDIR)/Windd.lib" 
LIB32_OBJS= \
	"$(INTDIR)\ACMUTIL.OBJ" \
	"$(INTDIR)\APPPALET.OBJ" \
	"$(INTDIR)\BITBUFFE.OBJ" \
	"$(INTDIR)\BLT.OBJ" \
	"$(INTDIR)\BPECODEC.OBJ" \
	"$(INTDIR)\BTREE.OBJ" \
	"$(INTDIR)\cache.obj" \
	"$(INTDIR)\codec.obj" \
	"$(INTDIR)\DATAFILE.OBJ" \
	"$(INTDIR)\DAVIDINL.OBJ" \
	"$(INTDIR)\DIB.OBJ" \
	"$(INTDIR)\DIBWND.OBJ" \
	"$(INTDIR)\DlgMsg.obj" \
	"$(INTDIR)\FIXPOINT.OBJ" \
	"$(INTDIR)\Flcanim.obj" \
	"$(INTDIR)\Flcctrl.obj" \
	"$(INTDIR)\GLOBAL.OBJ" \
	"$(INTDIR)\HUFFMANC.OBJ" \
	"$(INTDIR)\INIT.OBJ" \
	"$(INTDIR)\logging.obj" \
	"$(INTDIR)\LZSSCODE.OBJ" \
	"$(INTDIR)\LZWCODEC.OBJ" \
	"$(INTDIR)\MMIO.OBJ" \
	"$(INTDIR)\MSG_BOX.OBJ" \
	"$(INTDIR)\MUSIC.OBJ" \
	"$(INTDIR)\RAND.OBJ" \
	"$(INTDIR)\SCANLIST.OBJ" \
	"$(INTDIR)\STDAFX.OBJ" \
	"$(INTDIR)\STREXT.OBJ" \
	"$(INTDIR)\Subclass.obj" \
	"$(INTDIR)\THREADS.OBJ" \
	"$(INTDIR)\wndbase.obj"

"$(OUTDIR)\Windd.lib" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"

# PROP BASE Use_MFC 1
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Windward"
# PROP BASE Intermediate_Dir "Windward"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "ReleaseDLL"
# PROP Intermediate_Dir "ReleaseDLL"
# PROP Target_Dir ""
OUTDIR=.\ReleaseDLL
INTDIR=.\ReleaseDLL

ALL : "$(OUTDIR)\windDLL.lib" "$(OUTDIR)\wind40.bsc"

CLEAN : 
	-@erase "$(INTDIR)\ACMUTIL.OBJ"
	-@erase "$(INTDIR)\ACMUTIL.SBR"
	-@erase "$(INTDIR)\APPPALET.OBJ"
	-@erase "$(INTDIR)\APPPALET.SBR"
	-@erase "$(INTDIR)\BITBUFFE.OBJ"
	-@erase "$(INTDIR)\BITBUFFE.SBR"
	-@erase "$(INTDIR)\BLT.OBJ"
	-@erase "$(INTDIR)\BLT.SBR"
	-@erase "$(INTDIR)\BPECODEC.OBJ"
	-@erase "$(INTDIR)\BPECODEC.SBR"
	-@erase "$(INTDIR)\BTREE.OBJ"
	-@erase "$(INTDIR)\BTREE.SBR"
	-@erase "$(INTDIR)\cache.obj"
	-@erase "$(INTDIR)\cache.sbr"
	-@erase "$(INTDIR)\codec.obj"
	-@erase "$(INTDIR)\codec.sbr"
	-@erase "$(INTDIR)\DATAFILE.OBJ"
	-@erase "$(INTDIR)\DATAFILE.SBR"
	-@erase "$(INTDIR)\DAVIDINL.OBJ"
	-@erase "$(INTDIR)\DAVIDINL.SBR"
	-@erase "$(INTDIR)\DIB.OBJ"
	-@erase "$(INTDIR)\DIB.SBR"
	-@erase "$(INTDIR)\DIBWND.OBJ"
	-@erase "$(INTDIR)\DIBWND.SBR"
	-@erase "$(INTDIR)\DlgMsg.obj"
	-@erase "$(INTDIR)\DlgMsg.sbr"
	-@erase "$(INTDIR)\FIXPOINT.OBJ"
	-@erase "$(INTDIR)\FIXPOINT.SBR"
	-@erase "$(INTDIR)\Flcanim.obj"
	-@erase "$(INTDIR)\Flcanim.sbr"
	-@erase "$(INTDIR)\Flcctrl.obj"
	-@erase "$(INTDIR)\Flcctrl.sbr"
	-@erase "$(INTDIR)\GLOBAL.OBJ"
	-@erase "$(INTDIR)\GLOBAL.SBR"
	-@erase "$(INTDIR)\HUFFMANC.OBJ"
	-@erase "$(INTDIR)\HUFFMANC.SBR"
	-@erase "$(INTDIR)\INIT.OBJ"
	-@erase "$(INTDIR)\INIT.SBR"
	-@erase "$(INTDIR)\logging.obj"
	-@erase "$(INTDIR)\logging.sbr"
	-@erase "$(INTDIR)\LZSSCODE.OBJ"
	-@erase "$(INTDIR)\LZSSCODE.SBR"
	-@erase "$(INTDIR)\LZWCODEC.OBJ"
	-@erase "$(INTDIR)\LZWCODEC.SBR"
	-@erase "$(INTDIR)\MMIO.OBJ"
	-@erase "$(INTDIR)\MMIO.SBR"
	-@erase "$(INTDIR)\MSG_BOX.OBJ"
	-@erase "$(INTDIR)\MSG_BOX.SBR"
	-@erase "$(INTDIR)\MUSIC.OBJ"
	-@erase "$(INTDIR)\MUSIC.SBR"
	-@erase "$(INTDIR)\RAND.OBJ"
	-@erase "$(INTDIR)\RAND.SBR"
	-@erase "$(INTDIR)\SCANLIST.OBJ"
	-@erase "$(INTDIR)\SCANLIST.SBR"
	-@erase "$(INTDIR)\STDAFX.OBJ"
	-@erase "$(INTDIR)\STDAFX.SBR"
	-@erase "$(INTDIR)\STREXT.OBJ"
	-@erase "$(INTDIR)\STREXT.SBR"
	-@erase "$(INTDIR)\Subclass.obj"
	-@erase "$(INTDIR)\Subclass.sbr"
	-@erase "$(INTDIR)\THREADS.OBJ"
	-@erase "$(INTDIR)\THREADS.SBR"
	-@erase "$(INTDIR)\vc40.pdb"
	-@erase "$(INTDIR)\wndbase.obj"
	-@erase "$(INTDIR)\wndbase.sbr"
	-@erase "$(OUTDIR)\wind40.bsc"
	-@erase "$(OUTDIR)\windDLL.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE CPP /nologo /MT /W3 /GX /Zi /O2 /Ob2 /I "..\include" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /FR /YX /c
# ADD CPP /nologo /MD /W3 /GX /Zi /O2 /Ob2 /I "..\include" /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /YX /c
CPP_PROJ=/nologo /MD /W3 /GX /Zi /O2 /Ob2 /I "..\include" /D "NDEBUG" /D\
 "WIN32" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR"$(INTDIR)/"\
 /Fp"$(INTDIR)/wind40.pch" /YX /Fo"$(INTDIR)/" /Fd"$(INTDIR)/" /c 
CPP_OBJS=.\ReleaseDLL/
CPP_SBRS=.\ReleaseDLL/
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/wind40.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\ACMUTIL.SBR" \
	"$(INTDIR)\APPPALET.SBR" \
	"$(INTDIR)\BITBUFFE.SBR" \
	"$(INTDIR)\BLT.SBR" \
	"$(INTDIR)\BPECODEC.SBR" \
	"$(INTDIR)\BTREE.SBR" \
	"$(INTDIR)\cache.sbr" \
	"$(INTDIR)\codec.sbr" \
	"$(INTDIR)\DATAFILE.SBR" \
	"$(INTDIR)\DAVIDINL.SBR" \
	"$(INTDIR)\DIB.SBR" \
	"$(INTDIR)\DIBWND.SBR" \
	"$(INTDIR)\DlgMsg.sbr" \
	"$(INTDIR)\FIXPOINT.SBR" \
	"$(INTDIR)\Flcanim.sbr" \
	"$(INTDIR)\Flcctrl.sbr" \
	"$(INTDIR)\GLOBAL.SBR" \
	"$(INTDIR)\HUFFMANC.SBR" \
	"$(INTDIR)\INIT.SBR" \
	"$(INTDIR)\logging.sbr" \
	"$(INTDIR)\LZSSCODE.SBR" \
	"$(INTDIR)\LZWCODEC.SBR" \
	"$(INTDIR)\MMIO.SBR" \
	"$(INTDIR)\MSG_BOX.SBR" \
	"$(INTDIR)\MUSIC.SBR" \
	"$(INTDIR)\RAND.SBR" \
	"$(INTDIR)\SCANLIST.SBR" \
	"$(INTDIR)\STDAFX.SBR" \
	"$(INTDIR)\STREXT.SBR" \
	"$(INTDIR)\Subclass.sbr" \
	"$(INTDIR)\THREADS.SBR" \
	"$(INTDIR)\wndbase.sbr"

"$(OUTDIR)\wind40.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo /out:"ReleaseDLL\windDLL.lib"
LIB32_FLAGS=/nologo /out:"$(OUTDIR)/windDLL.lib" 
LIB32_OBJS= \
	"$(INTDIR)\ACMUTIL.OBJ" \
	"$(INTDIR)\APPPALET.OBJ" \
	"$(INTDIR)\BITBUFFE.OBJ" \
	"$(INTDIR)\BLT.OBJ" \
	"$(INTDIR)\BPECODEC.OBJ" \
	"$(INTDIR)\BTREE.OBJ" \
	"$(INTDIR)\cache.obj" \
	"$(INTDIR)\codec.obj" \
	"$(INTDIR)\DATAFILE.OBJ" \
	"$(INTDIR)\DAVIDINL.OBJ" \
	"$(INTDIR)\DIB.OBJ" \
	"$(INTDIR)\DIBWND.OBJ" \
	"$(INTDIR)\DlgMsg.obj" \
	"$(INTDIR)\FIXPOINT.OBJ" \
	"$(INTDIR)\Flcanim.obj" \
	"$(INTDIR)\Flcctrl.obj" \
	"$(INTDIR)\GLOBAL.OBJ" \
	"$(INTDIR)\HUFFMANC.OBJ" \
	"$(INTDIR)\INIT.OBJ" \
	"$(INTDIR)\logging.obj" \
	"$(INTDIR)\LZSSCODE.OBJ" \
	"$(INTDIR)\LZWCODEC.OBJ" \
	"$(INTDIR)\MMIO.OBJ" \
	"$(INTDIR)\MSG_BOX.OBJ" \
	"$(INTDIR)\MUSIC.OBJ" \
	"$(INTDIR)\RAND.OBJ" \
	"$(INTDIR)\SCANLIST.OBJ" \
	"$(INTDIR)\STDAFX.OBJ" \
	"$(INTDIR)\STREXT.OBJ" \
	"$(INTDIR)\Subclass.obj" \
	"$(INTDIR)\THREADS.OBJ" \
	"$(INTDIR)\wndbase.obj"

"$(OUTDIR)\windDLL.lib" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"

# PROP BASE Use_MFC 1
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Windwar0"
# PROP BASE Intermediate_Dir "Windwar0"
# PROP BASE Target_Dir ""
# PROP Use_MFC 2
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "DebugDLL"
# PROP Intermediate_Dir "DebugDLL"
# PROP Target_Dir ""
OUTDIR=.\DebugDLL
INTDIR=.\DebugDLL

ALL : "$(OUTDIR)\WindDLLd.lib" "$(OUTDIR)\wind40.bsc"

CLEAN : 
	-@erase "$(INTDIR)\ACMUTIL.OBJ"
	-@erase "$(INTDIR)\ACMUTIL.SBR"
	-@erase "$(INTDIR)\APPPALET.OBJ"
	-@erase "$(INTDIR)\APPPALET.SBR"
	-@erase "$(INTDIR)\BITBUFFE.OBJ"
	-@erase "$(INTDIR)\BITBUFFE.SBR"
	-@erase "$(INTDIR)\BLT.OBJ"
	-@erase "$(INTDIR)\BLT.SBR"
	-@erase "$(INTDIR)\BPECODEC.OBJ"
	-@erase "$(INTDIR)\BPECODEC.SBR"
	-@erase "$(INTDIR)\BTREE.OBJ"
	-@erase "$(INTDIR)\BTREE.SBR"
	-@erase "$(INTDIR)\cache.obj"
	-@erase "$(INTDIR)\cache.sbr"
	-@erase "$(INTDIR)\codec.obj"
	-@erase "$(INTDIR)\codec.sbr"
	-@erase "$(INTDIR)\DATAFILE.OBJ"
	-@erase "$(INTDIR)\DATAFILE.SBR"
	-@erase "$(INTDIR)\DAVIDINL.OBJ"
	-@erase "$(INTDIR)\DAVIDINL.SBR"
	-@erase "$(INTDIR)\DIB.OBJ"
	-@erase "$(INTDIR)\DIB.SBR"
	-@erase "$(INTDIR)\DIBWND.OBJ"
	-@erase "$(INTDIR)\DIBWND.SBR"
	-@erase "$(INTDIR)\DlgMsg.obj"
	-@erase "$(INTDIR)\DlgMsg.sbr"
	-@erase "$(INTDIR)\FIXPOINT.OBJ"
	-@erase "$(INTDIR)\FIXPOINT.SBR"
	-@erase "$(INTDIR)\Flcanim.obj"
	-@erase "$(INTDIR)\Flcanim.sbr"
	-@erase "$(INTDIR)\Flcctrl.obj"
	-@erase "$(INTDIR)\Flcctrl.sbr"
	-@erase "$(INTDIR)\GLOBAL.OBJ"
	-@erase "$(INTDIR)\GLOBAL.SBR"
	-@erase "$(INTDIR)\HUFFMANC.OBJ"
	-@erase "$(INTDIR)\HUFFMANC.SBR"
	-@erase "$(INTDIR)\INIT.OBJ"
	-@erase "$(INTDIR)\INIT.SBR"
	-@erase "$(INTDIR)\logging.obj"
	-@erase "$(INTDIR)\logging.sbr"
	-@erase "$(INTDIR)\LZSSCODE.OBJ"
	-@erase "$(INTDIR)\LZSSCODE.SBR"
	-@erase "$(INTDIR)\LZWCODEC.OBJ"
	-@erase "$(INTDIR)\LZWCODEC.SBR"
	-@erase "$(INTDIR)\MMIO.OBJ"
	-@erase "$(INTDIR)\MMIO.SBR"
	-@erase "$(INTDIR)\MSG_BOX.OBJ"
	-@erase "$(INTDIR)\MSG_BOX.SBR"
	-@erase "$(INTDIR)\MUSIC.OBJ"
	-@erase "$(INTDIR)\MUSIC.SBR"
	-@erase "$(INTDIR)\RAND.OBJ"
	-@erase "$(INTDIR)\RAND.SBR"
	-@erase "$(INTDIR)\SCANLIST.OBJ"
	-@erase "$(INTDIR)\SCANLIST.SBR"
	-@erase "$(INTDIR)\STDAFX.OBJ"
	-@erase "$(INTDIR)\STDAFX.SBR"
	-@erase "$(INTDIR)\STREXT.OBJ"
	-@erase "$(INTDIR)\STREXT.SBR"
	-@erase "$(INTDIR)\Subclass.obj"
	-@erase "$(INTDIR)\Subclass.sbr"
	-@erase "$(INTDIR)\THREADS.OBJ"
	-@erase "$(INTDIR)\THREADS.SBR"
	-@erase "$(INTDIR)\vc40.idb"
	-@erase "$(INTDIR)\vc40.pdb"
	-@erase "$(INTDIR)\wndbase.obj"
	-@erase "$(INTDIR)\wndbase.sbr"
	-@erase "$(OUTDIR)\wind40.bsc"
	-@erase "$(OUTDIR)\WindDLLd.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /Zi /Od /I "..\include" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_CHEAT" /D "_MBCS" /FR /YX /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /Zi /Od /I "..\include" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_CHEAT" /D "_AFXDLL" /D "_MBCS" /FR /YX /c
CPP_PROJ=/nologo /MDd /W3 /Gm /GX /Zi /Od /I "..\include" /D "_DEBUG" /D\
 "WIN32" /D "_WINDOWS" /D "_CHEAT" /D "_AFXDLL" /D "_MBCS" /FR"$(INTDIR)/"\
 /Fp"$(INTDIR)/wind40.pch" /YX /Fo"$(INTDIR)/" /Fd"$(INTDIR)/" /c 
CPP_OBJS=.\DebugDLL/
CPP_SBRS=.\DebugDLL/
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
BSC32_FLAGS=/nologo /o"$(OUTDIR)/wind40.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\ACMUTIL.SBR" \
	"$(INTDIR)\APPPALET.SBR" \
	"$(INTDIR)\BITBUFFE.SBR" \
	"$(INTDIR)\BLT.SBR" \
	"$(INTDIR)\BPECODEC.SBR" \
	"$(INTDIR)\BTREE.SBR" \
	"$(INTDIR)\cache.sbr" \
	"$(INTDIR)\codec.sbr" \
	"$(INTDIR)\DATAFILE.SBR" \
	"$(INTDIR)\DAVIDINL.SBR" \
	"$(INTDIR)\DIB.SBR" \
	"$(INTDIR)\DIBWND.SBR" \
	"$(INTDIR)\DlgMsg.sbr" \
	"$(INTDIR)\FIXPOINT.SBR" \
	"$(INTDIR)\Flcanim.sbr" \
	"$(INTDIR)\Flcctrl.sbr" \
	"$(INTDIR)\GLOBAL.SBR" \
	"$(INTDIR)\HUFFMANC.SBR" \
	"$(INTDIR)\INIT.SBR" \
	"$(INTDIR)\logging.sbr" \
	"$(INTDIR)\LZSSCODE.SBR" \
	"$(INTDIR)\LZWCODEC.SBR" \
	"$(INTDIR)\MMIO.SBR" \
	"$(INTDIR)\MSG_BOX.SBR" \
	"$(INTDIR)\MUSIC.SBR" \
	"$(INTDIR)\RAND.SBR" \
	"$(INTDIR)\SCANLIST.SBR" \
	"$(INTDIR)\STDAFX.SBR" \
	"$(INTDIR)\STREXT.SBR" \
	"$(INTDIR)\Subclass.sbr" \
	"$(INTDIR)\THREADS.SBR" \
	"$(INTDIR)\wndbase.sbr"

"$(OUTDIR)\wind40.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LIB32=link.exe -lib
# ADD BASE LIB32 /nologo /out:"WinDeb40\Wind40d.lib"
# ADD LIB32 /nologo /out:"debugdll\WindDLLd.lib"
LIB32_FLAGS=/nologo /out:"$(OUTDIR)/WindDLLd.lib" 
LIB32_OBJS= \
	"$(INTDIR)\ACMUTIL.OBJ" \
	"$(INTDIR)\APPPALET.OBJ" \
	"$(INTDIR)\BITBUFFE.OBJ" \
	"$(INTDIR)\BLT.OBJ" \
	"$(INTDIR)\BPECODEC.OBJ" \
	"$(INTDIR)\BTREE.OBJ" \
	"$(INTDIR)\cache.obj" \
	"$(INTDIR)\codec.obj" \
	"$(INTDIR)\DATAFILE.OBJ" \
	"$(INTDIR)\DAVIDINL.OBJ" \
	"$(INTDIR)\DIB.OBJ" \
	"$(INTDIR)\DIBWND.OBJ" \
	"$(INTDIR)\DlgMsg.obj" \
	"$(INTDIR)\FIXPOINT.OBJ" \
	"$(INTDIR)\Flcanim.obj" \
	"$(INTDIR)\Flcctrl.obj" \
	"$(INTDIR)\GLOBAL.OBJ" \
	"$(INTDIR)\HUFFMANC.OBJ" \
	"$(INTDIR)\INIT.OBJ" \
	"$(INTDIR)\logging.obj" \
	"$(INTDIR)\LZSSCODE.OBJ" \
	"$(INTDIR)\LZWCODEC.OBJ" \
	"$(INTDIR)\MMIO.OBJ" \
	"$(INTDIR)\MSG_BOX.OBJ" \
	"$(INTDIR)\MUSIC.OBJ" \
	"$(INTDIR)\RAND.OBJ" \
	"$(INTDIR)\SCANLIST.OBJ" \
	"$(INTDIR)\STDAFX.OBJ" \
	"$(INTDIR)\STREXT.OBJ" \
	"$(INTDIR)\Subclass.obj" \
	"$(INTDIR)\THREADS.OBJ" \
	"$(INTDIR)\wndbase.obj"

"$(OUTDIR)\WindDLLd.lib" : "$(OUTDIR)" $(DEF_FILE) $(LIB32_OBJS)
    $(LIB32) @<<
  $(LIB32_FLAGS) $(DEF_FLAGS) $(LIB32_OBJS)
<<

!ENDIF 

.c{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.cpp{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.cxx{$(CPP_OBJS)}.obj:
   $(CPP) $(CPP_PROJ) $<  

.c{$(CPP_SBRS)}.sbr:
   $(CPP) $(CPP_PROJ) $<  

.cpp{$(CPP_SBRS)}.sbr:
   $(CPP) $(CPP_PROJ) $<  

.cxx{$(CPP_SBRS)}.sbr:
   $(CPP) $(CPP_PROJ) $<  

################################################################################
# Begin Target

# Name "Windward - Win32 Release"
# Name "Windward - Win32 Debug"
# Name "Windward - Win32 Release_dll"
# Name "Windward - Win32 Debug_dll"

!IF  "$(CFG)" == "Windward - Win32 Release"

!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"

!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"

!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"

!ENDIF 

################################################################################
# Begin Source File

SOURCE=.\MSG_BOX.CPP
DEP_CPP_MSG_B=\
	"..\include\_debug.h"\
	"..\include\_error.h"\
	"..\include\_help.h"\
	"..\include\_msgs.h"\
	"..\include\_windwrd.h"\
	"..\include\blt.h"\
	"..\include\btree.h"\
	"..\include\cache.h"\
	"..\include\codec.h"\
	"..\include\datafile.h"\
	"..\include\dib.h"\
	"..\include\dibwnd.h"\
	"..\include\DlgMsg.h"\
	"..\include\fixpoint.h"\
	"..\include\games.h"\
	"..\include\init.h"\
	"..\include\logging.h"\
	"..\include\mmio.h"\
	"..\include\mmio.inl"\
	"..\include\ptr.h"\
	"..\include\rand.h"\
	"..\include\scanlist.h"\
	"..\include\subclass.h"\
	"..\include\thielen.h"\
	"..\include\thielen.inl"\
	"..\include\threads.h"\
	"..\include\windward.h"\
	"..\include\windward.inl"\
	"..\include\wndbase.h"\
	".\..\include\dave32ut.h"\
	".\..\include\gg.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

!IF  "$(CFG)" == "Windward - Win32 Release"


"$(INTDIR)\MSG_BOX.OBJ" : $(SOURCE) $(DEP_CPP_MSG_B) "$(INTDIR)"

"$(INTDIR)\MSG_BOX.SBR" : $(SOURCE) $(DEP_CPP_MSG_B) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"


"$(INTDIR)\MSG_BOX.OBJ" : $(SOURCE) $(DEP_CPP_MSG_B) "$(INTDIR)"

"$(INTDIR)\MSG_BOX.SBR" : $(SOURCE) $(DEP_CPP_MSG_B) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"


"$(INTDIR)\MSG_BOX.OBJ" : $(SOURCE) $(DEP_CPP_MSG_B) "$(INTDIR)"

"$(INTDIR)\MSG_BOX.SBR" : $(SOURCE) $(DEP_CPP_MSG_B) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"


"$(INTDIR)\MSG_BOX.OBJ" : $(SOURCE) $(DEP_CPP_MSG_B) "$(INTDIR)"

"$(INTDIR)\MSG_BOX.SBR" : $(SOURCE) $(DEP_CPP_MSG_B) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\THREADS.CPP
DEP_CPP_THREA=\
	"..\include\_debug.h"\
	"..\include\_error.h"\
	"..\include\_help.h"\
	"..\include\_msgs.h"\
	"..\include\_windwrd.h"\
	"..\include\blt.h"\
	"..\include\btree.h"\
	"..\include\cache.h"\
	"..\include\codec.h"\
	"..\include\datafile.h"\
	"..\include\dib.h"\
	"..\include\dibwnd.h"\
	"..\include\DlgMsg.h"\
	"..\include\fixpoint.h"\
	"..\include\games.h"\
	"..\include\init.h"\
	"..\include\logging.h"\
	"..\include\mmio.h"\
	"..\include\mmio.inl"\
	"..\include\ptr.h"\
	"..\include\rand.h"\
	"..\include\scanlist.h"\
	"..\include\subclass.h"\
	"..\include\thielen.h"\
	"..\include\thielen.inl"\
	"..\include\threads.h"\
	"..\include\windward.h"\
	"..\include\windward.inl"\
	"..\include\wndbase.h"\
	".\..\include\dave32ut.h"\
	".\..\include\gg.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

!IF  "$(CFG)" == "Windward - Win32 Release"


"$(INTDIR)\THREADS.OBJ" : $(SOURCE) $(DEP_CPP_THREA) "$(INTDIR)"

"$(INTDIR)\THREADS.SBR" : $(SOURCE) $(DEP_CPP_THREA) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"


"$(INTDIR)\THREADS.OBJ" : $(SOURCE) $(DEP_CPP_THREA) "$(INTDIR)"

"$(INTDIR)\THREADS.SBR" : $(SOURCE) $(DEP_CPP_THREA) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"


"$(INTDIR)\THREADS.OBJ" : $(SOURCE) $(DEP_CPP_THREA) "$(INTDIR)"

"$(INTDIR)\THREADS.SBR" : $(SOURCE) $(DEP_CPP_THREA) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"


"$(INTDIR)\THREADS.OBJ" : $(SOURCE) $(DEP_CPP_THREA) "$(INTDIR)"

"$(INTDIR)\THREADS.SBR" : $(SOURCE) $(DEP_CPP_THREA) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\STDAFX.CPP

!IF  "$(CFG)" == "Windward - Win32 Release"

DEP_CPP_STDAF=\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

"$(INTDIR)\STDAFX.OBJ" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"

"$(INTDIR)\STDAFX.SBR" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"

DEP_CPP_STDAF=\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

"$(INTDIR)\STDAFX.OBJ" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"

"$(INTDIR)\STDAFX.SBR" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"

DEP_CPP_STDAF=\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

"$(INTDIR)\STDAFX.OBJ" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"

"$(INTDIR)\STDAFX.SBR" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"

DEP_CPP_STDAF=\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

"$(INTDIR)\STDAFX.OBJ" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"

"$(INTDIR)\STDAFX.SBR" : $(SOURCE) $(DEP_CPP_STDAF) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\GLOBAL.CPP
DEP_CPP_GLOBA=\
	"..\include\_debug.h"\
	"..\include\_error.h"\
	"..\include\_help.h"\
	"..\include\_msgs.h"\
	"..\include\_windwrd.h"\
	"..\include\blt.h"\
	"..\include\btree.h"\
	"..\include\cache.h"\
	"..\include\codec.h"\
	"..\include\datafile.h"\
	"..\include\dib.h"\
	"..\include\dibwnd.h"\
	"..\include\DlgMsg.h"\
	"..\include\fixpoint.h"\
	"..\include\games.h"\
	"..\include\init.h"\
	"..\include\logging.h"\
	"..\include\mmio.h"\
	"..\include\mmio.inl"\
	"..\include\ptr.h"\
	"..\include\rand.h"\
	"..\include\scanlist.h"\
	"..\include\subclass.h"\
	"..\include\thielen.h"\
	"..\include\thielen.inl"\
	"..\include\threads.h"\
	"..\include\windward.h"\
	"..\include\windward.inl"\
	"..\include\wndbase.h"\
	".\..\include\dave32ut.h"\
	".\..\include\gg.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

!IF  "$(CFG)" == "Windward - Win32 Release"


"$(INTDIR)\GLOBAL.OBJ" : $(SOURCE) $(DEP_CPP_GLOBA) "$(INTDIR)"

"$(INTDIR)\GLOBAL.SBR" : $(SOURCE) $(DEP_CPP_GLOBA) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"


"$(INTDIR)\GLOBAL.OBJ" : $(SOURCE) $(DEP_CPP_GLOBA) "$(INTDIR)"

"$(INTDIR)\GLOBAL.SBR" : $(SOURCE) $(DEP_CPP_GLOBA) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"


"$(INTDIR)\GLOBAL.OBJ" : $(SOURCE) $(DEP_CPP_GLOBA) "$(INTDIR)"

"$(INTDIR)\GLOBAL.SBR" : $(SOURCE) $(DEP_CPP_GLOBA) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"


"$(INTDIR)\GLOBAL.OBJ" : $(SOURCE) $(DEP_CPP_GLOBA) "$(INTDIR)"

"$(INTDIR)\GLOBAL.SBR" : $(SOURCE) $(DEP_CPP_GLOBA) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\MMIO.CPP
DEP_CPP_MMIO_=\
	"..\include\_debug.h"\
	"..\include\_error.h"\
	"..\include\_help.h"\
	"..\include\_msgs.h"\
	"..\include\_windwrd.h"\
	"..\include\blt.h"\
	"..\include\btree.h"\
	"..\include\cache.h"\
	"..\include\codec.h"\
	"..\include\datafile.h"\
	"..\include\dib.h"\
	"..\include\dibwnd.h"\
	"..\include\DlgMsg.h"\
	"..\include\fixpoint.h"\
	"..\include\games.h"\
	"..\include\init.h"\
	"..\include\logging.h"\
	"..\include\mmio.h"\
	"..\include\mmio.inl"\
	"..\include\ptr.h"\
	"..\include\rand.h"\
	"..\include\scanlist.h"\
	"..\include\subclass.h"\
	"..\include\thielen.h"\
	"..\include\thielen.inl"\
	"..\include\threads.h"\
	"..\include\windward.h"\
	"..\include\windward.inl"\
	"..\include\wndbase.h"\
	".\..\include\dave32ut.h"\
	".\..\include\gg.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

!IF  "$(CFG)" == "Windward - Win32 Release"


"$(INTDIR)\MMIO.OBJ" : $(SOURCE) $(DEP_CPP_MMIO_) "$(INTDIR)"

"$(INTDIR)\MMIO.SBR" : $(SOURCE) $(DEP_CPP_MMIO_) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"


"$(INTDIR)\MMIO.OBJ" : $(SOURCE) $(DEP_CPP_MMIO_) "$(INTDIR)"

"$(INTDIR)\MMIO.SBR" : $(SOURCE) $(DEP_CPP_MMIO_) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"


"$(INTDIR)\MMIO.OBJ" : $(SOURCE) $(DEP_CPP_MMIO_) "$(INTDIR)"

"$(INTDIR)\MMIO.SBR" : $(SOURCE) $(DEP_CPP_MMIO_) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"


"$(INTDIR)\MMIO.OBJ" : $(SOURCE) $(DEP_CPP_MMIO_) "$(INTDIR)"

"$(INTDIR)\MMIO.SBR" : $(SOURCE) $(DEP_CPP_MMIO_) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\INIT.CPP
DEP_CPP_INIT_=\
	"..\include\_debug.h"\
	"..\include\_error.h"\
	"..\include\_help.h"\
	"..\include\_msgs.h"\
	"..\include\_windwrd.h"\
	"..\include\blt.h"\
	"..\include\btree.h"\
	"..\include\cache.h"\
	"..\include\codec.h"\
	"..\include\datafile.h"\
	"..\include\dib.h"\
	"..\include\dibwnd.h"\
	"..\include\DlgMsg.h"\
	"..\include\fixpoint.h"\
	"..\include\games.h"\
	"..\include\init.h"\
	"..\include\logging.h"\
	"..\include\mmio.h"\
	"..\include\mmio.inl"\
	"..\include\ptr.h"\
	"..\include\rand.h"\
	"..\include\scanlist.h"\
	"..\include\subclass.h"\
	"..\include\thielen.h"\
	"..\include\thielen.inl"\
	"..\include\threads.h"\
	"..\include\windward.h"\
	"..\include\windward.inl"\
	"..\include\wndbase.h"\
	".\..\include\dave32ut.h"\
	".\..\include\gg.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

!IF  "$(CFG)" == "Windward - Win32 Release"


"$(INTDIR)\INIT.OBJ" : $(SOURCE) $(DEP_CPP_INIT_) "$(INTDIR)"

"$(INTDIR)\INIT.SBR" : $(SOURCE) $(DEP_CPP_INIT_) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"


"$(INTDIR)\INIT.OBJ" : $(SOURCE) $(DEP_CPP_INIT_) "$(INTDIR)"

"$(INTDIR)\INIT.SBR" : $(SOURCE) $(DEP_CPP_INIT_) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"


"$(INTDIR)\INIT.OBJ" : $(SOURCE) $(DEP_CPP_INIT_) "$(INTDIR)"

"$(INTDIR)\INIT.SBR" : $(SOURCE) $(DEP_CPP_INIT_) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"


"$(INTDIR)\INIT.OBJ" : $(SOURCE) $(DEP_CPP_INIT_) "$(INTDIR)"

"$(INTDIR)\INIT.SBR" : $(SOURCE) $(DEP_CPP_INIT_) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\RAND.CPP
DEP_CPP_RAND_=\
	"..\include\_debug.h"\
	"..\include\_error.h"\
	"..\include\_help.h"\
	"..\include\_msgs.h"\
	"..\include\_windwrd.h"\
	"..\include\blt.h"\
	"..\include\btree.h"\
	"..\include\cache.h"\
	"..\include\codec.h"\
	"..\include\datafile.h"\
	"..\include\dib.h"\
	"..\include\dibwnd.h"\
	"..\include\DlgMsg.h"\
	"..\include\fixpoint.h"\
	"..\include\games.h"\
	"..\include\init.h"\
	"..\include\logging.h"\
	"..\include\mmio.h"\
	"..\include\mmio.inl"\
	"..\include\ptr.h"\
	"..\include\rand.h"\
	"..\include\scanlist.h"\
	"..\include\subclass.h"\
	"..\include\thielen.h"\
	"..\include\thielen.inl"\
	"..\include\threads.h"\
	"..\include\windward.h"\
	"..\include\windward.inl"\
	"..\include\wndbase.h"\
	".\..\include\dave32ut.h"\
	".\..\include\gg.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

!IF  "$(CFG)" == "Windward - Win32 Release"


"$(INTDIR)\RAND.OBJ" : $(SOURCE) $(DEP_CPP_RAND_) "$(INTDIR)"

"$(INTDIR)\RAND.SBR" : $(SOURCE) $(DEP_CPP_RAND_) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"


"$(INTDIR)\RAND.OBJ" : $(SOURCE) $(DEP_CPP_RAND_) "$(INTDIR)"

"$(INTDIR)\RAND.SBR" : $(SOURCE) $(DEP_CPP_RAND_) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"


"$(INTDIR)\RAND.OBJ" : $(SOURCE) $(DEP_CPP_RAND_) "$(INTDIR)"

"$(INTDIR)\RAND.SBR" : $(SOURCE) $(DEP_CPP_RAND_) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"


"$(INTDIR)\RAND.OBJ" : $(SOURCE) $(DEP_CPP_RAND_) "$(INTDIR)"

"$(INTDIR)\RAND.SBR" : $(SOURCE) $(DEP_CPP_RAND_) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\DAVIDINL.CPP
DEP_CPP_DAVID=\
	"..\include\_debug.h"\
	"..\include\_error.h"\
	"..\include\_help.h"\
	"..\include\_msgs.h"\
	"..\include\_windwrd.h"\
	"..\include\blt.h"\
	"..\include\btree.h"\
	"..\include\cache.h"\
	"..\include\codec.h"\
	"..\include\datafile.h"\
	"..\include\dib.h"\
	"..\include\dibwnd.h"\
	"..\include\DlgMsg.h"\
	"..\include\fixpoint.h"\
	"..\include\games.h"\
	"..\include\init.h"\
	"..\include\logging.h"\
	"..\include\mmio.h"\
	"..\include\mmio.inl"\
	"..\include\ptr.h"\
	"..\include\rand.h"\
	"..\include\scanlist.h"\
	"..\include\subclass.h"\
	"..\include\thielen.h"\
	"..\include\thielen.inl"\
	"..\include\threads.h"\
	"..\include\windward.h"\
	"..\include\windward.inl"\
	"..\include\wndbase.h"\
	".\..\include\dave32ut.h"\
	".\..\include\gg.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

!IF  "$(CFG)" == "Windward - Win32 Release"


"$(INTDIR)\DAVIDINL.OBJ" : $(SOURCE) $(DEP_CPP_DAVID) "$(INTDIR)"

"$(INTDIR)\DAVIDINL.SBR" : $(SOURCE) $(DEP_CPP_DAVID) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"


"$(INTDIR)\DAVIDINL.OBJ" : $(SOURCE) $(DEP_CPP_DAVID) "$(INTDIR)"

"$(INTDIR)\DAVIDINL.SBR" : $(SOURCE) $(DEP_CPP_DAVID) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"


"$(INTDIR)\DAVIDINL.OBJ" : $(SOURCE) $(DEP_CPP_DAVID) "$(INTDIR)"

"$(INTDIR)\DAVIDINL.SBR" : $(SOURCE) $(DEP_CPP_DAVID) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"


"$(INTDIR)\DAVIDINL.OBJ" : $(SOURCE) $(DEP_CPP_DAVID) "$(INTDIR)"

"$(INTDIR)\DAVIDINL.SBR" : $(SOURCE) $(DEP_CPP_DAVID) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\MUSIC.CPP
DEP_CPP_MUSIC=\
	"..\include\_debug.h"\
	"..\include\_error.h"\
	"..\include\_help.h"\
	"..\include\_msgs.h"\
	"..\include\_windwrd.h"\
	"..\include\acmutil.h"\
	"..\include\blt.h"\
	"..\include\btree.h"\
	"..\include\cache.h"\
	"..\include\codec.h"\
	"..\include\datafile.h"\
	"..\include\dib.h"\
	"..\include\dibwnd.h"\
	"..\include\DlgMsg.h"\
	"..\include\fixpoint.h"\
	"..\include\games.h"\
	"..\include\init.h"\
	"..\include\logging.h"\
	"..\include\mmio.h"\
	"..\include\mmio.inl"\
	"..\include\music.h"\
	"..\include\ptr.h"\
	"..\include\rand.h"\
	"..\include\scanlist.h"\
	"..\include\subclass.h"\
	"..\include\thielen.h"\
	"..\include\thielen.inl"\
	"..\include\threads.h"\
	"..\include\windward.h"\
	"..\include\windward.inl"\
	"..\include\wndbase.h"\
	".\..\include\dave32ut.h"\
	".\..\include\gg.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

!IF  "$(CFG)" == "Windward - Win32 Release"


"$(INTDIR)\MUSIC.OBJ" : $(SOURCE) $(DEP_CPP_MUSIC) "$(INTDIR)"

"$(INTDIR)\MUSIC.SBR" : $(SOURCE) $(DEP_CPP_MUSIC) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"


"$(INTDIR)\MUSIC.OBJ" : $(SOURCE) $(DEP_CPP_MUSIC) "$(INTDIR)"

"$(INTDIR)\MUSIC.SBR" : $(SOURCE) $(DEP_CPP_MUSIC) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"


"$(INTDIR)\MUSIC.OBJ" : $(SOURCE) $(DEP_CPP_MUSIC) "$(INTDIR)"

"$(INTDIR)\MUSIC.SBR" : $(SOURCE) $(DEP_CPP_MUSIC) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"


"$(INTDIR)\MUSIC.OBJ" : $(SOURCE) $(DEP_CPP_MUSIC) "$(INTDIR)"

"$(INTDIR)\MUSIC.SBR" : $(SOURCE) $(DEP_CPP_MUSIC) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\STREXT.CPP
DEP_CPP_STREX=\
	"..\include\_debug.h"\
	"..\include\_error.h"\
	"..\include\_help.h"\
	"..\include\_msgs.h"\
	"..\include\_windwrd.h"\
	"..\include\blt.h"\
	"..\include\btree.h"\
	"..\include\cache.h"\
	"..\include\codec.h"\
	"..\include\datafile.h"\
	"..\include\dib.h"\
	"..\include\dibwnd.h"\
	"..\include\DlgMsg.h"\
	"..\include\fixpoint.h"\
	"..\include\games.h"\
	"..\include\init.h"\
	"..\include\logging.h"\
	"..\include\mmio.h"\
	"..\include\mmio.inl"\
	"..\include\ptr.h"\
	"..\include\rand.h"\
	"..\include\scanlist.h"\
	"..\include\subclass.h"\
	"..\include\thielen.h"\
	"..\include\thielen.inl"\
	"..\include\threads.h"\
	"..\include\windward.h"\
	"..\include\windward.inl"\
	"..\include\wndbase.h"\
	".\..\include\dave32ut.h"\
	".\..\include\gg.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

!IF  "$(CFG)" == "Windward - Win32 Release"


"$(INTDIR)\STREXT.OBJ" : $(SOURCE) $(DEP_CPP_STREX) "$(INTDIR)"

"$(INTDIR)\STREXT.SBR" : $(SOURCE) $(DEP_CPP_STREX) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"


"$(INTDIR)\STREXT.OBJ" : $(SOURCE) $(DEP_CPP_STREX) "$(INTDIR)"

"$(INTDIR)\STREXT.SBR" : $(SOURCE) $(DEP_CPP_STREX) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"


"$(INTDIR)\STREXT.OBJ" : $(SOURCE) $(DEP_CPP_STREX) "$(INTDIR)"

"$(INTDIR)\STREXT.SBR" : $(SOURCE) $(DEP_CPP_STREX) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"


"$(INTDIR)\STREXT.OBJ" : $(SOURCE) $(DEP_CPP_STREX) "$(INTDIR)"

"$(INTDIR)\STREXT.SBR" : $(SOURCE) $(DEP_CPP_STREX) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\APPPALET.CPP
DEP_CPP_APPPA=\
	"..\include\_debug.h"\
	"..\include\_error.h"\
	"..\include\_help.h"\
	"..\include\_msgs.h"\
	"..\include\_windwrd.h"\
	"..\include\blt.h"\
	"..\include\btree.h"\
	"..\include\cache.h"\
	"..\include\codec.h"\
	"..\include\datafile.h"\
	"..\include\dib.h"\
	"..\include\dibwnd.h"\
	"..\include\DlgMsg.h"\
	"..\include\fixpoint.h"\
	"..\include\games.h"\
	"..\include\init.h"\
	"..\include\logging.h"\
	"..\include\mmio.h"\
	"..\include\mmio.inl"\
	"..\include\ptr.h"\
	"..\include\rand.h"\
	"..\include\scanlist.h"\
	"..\include\subclass.h"\
	"..\include\thielen.h"\
	"..\include\thielen.inl"\
	"..\include\threads.h"\
	"..\include\windward.h"\
	"..\include\windward.inl"\
	"..\include\wndbase.h"\
	".\..\include\dave32ut.h"\
	".\..\include\gg.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

!IF  "$(CFG)" == "Windward - Win32 Release"


"$(INTDIR)\APPPALET.OBJ" : $(SOURCE) $(DEP_CPP_APPPA) "$(INTDIR)"

"$(INTDIR)\APPPALET.SBR" : $(SOURCE) $(DEP_CPP_APPPA) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"


"$(INTDIR)\APPPALET.OBJ" : $(SOURCE) $(DEP_CPP_APPPA) "$(INTDIR)"

"$(INTDIR)\APPPALET.SBR" : $(SOURCE) $(DEP_CPP_APPPA) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"


"$(INTDIR)\APPPALET.OBJ" : $(SOURCE) $(DEP_CPP_APPPA) "$(INTDIR)"

"$(INTDIR)\APPPALET.SBR" : $(SOURCE) $(DEP_CPP_APPPA) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"


"$(INTDIR)\APPPALET.OBJ" : $(SOURCE) $(DEP_CPP_APPPA) "$(INTDIR)"

"$(INTDIR)\APPPALET.SBR" : $(SOURCE) $(DEP_CPP_APPPA) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\SCANLIST.CPP
DEP_CPP_SCANL=\
	"..\include\_debug.h"\
	"..\include\_error.h"\
	"..\include\_help.h"\
	"..\include\_msgs.h"\
	"..\include\_windwrd.h"\
	"..\include\blt.h"\
	"..\include\btree.h"\
	"..\include\cache.h"\
	"..\include\codec.h"\
	"..\include\datafile.h"\
	"..\include\dib.h"\
	"..\include\dibwnd.h"\
	"..\include\DlgMsg.h"\
	"..\include\fixpoint.h"\
	"..\include\games.h"\
	"..\include\init.h"\
	"..\include\logging.h"\
	"..\include\mmio.h"\
	"..\include\mmio.inl"\
	"..\include\ptr.h"\
	"..\include\rand.h"\
	"..\include\scanlist.h"\
	"..\include\subclass.h"\
	"..\include\thielen.h"\
	"..\include\thielen.inl"\
	"..\include\threads.h"\
	"..\include\windward.h"\
	"..\include\windward.inl"\
	"..\include\wndbase.h"\
	".\..\include\dave32ut.h"\
	".\..\include\gg.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

!IF  "$(CFG)" == "Windward - Win32 Release"


"$(INTDIR)\SCANLIST.OBJ" : $(SOURCE) $(DEP_CPP_SCANL) "$(INTDIR)"

"$(INTDIR)\SCANLIST.SBR" : $(SOURCE) $(DEP_CPP_SCANL) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"


"$(INTDIR)\SCANLIST.OBJ" : $(SOURCE) $(DEP_CPP_SCANL) "$(INTDIR)"

"$(INTDIR)\SCANLIST.SBR" : $(SOURCE) $(DEP_CPP_SCANL) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"


"$(INTDIR)\SCANLIST.OBJ" : $(SOURCE) $(DEP_CPP_SCANL) "$(INTDIR)"

"$(INTDIR)\SCANLIST.SBR" : $(SOURCE) $(DEP_CPP_SCANL) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"


"$(INTDIR)\SCANLIST.OBJ" : $(SOURCE) $(DEP_CPP_SCANL) "$(INTDIR)"

"$(INTDIR)\SCANLIST.SBR" : $(SOURCE) $(DEP_CPP_SCANL) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\DATAFILE.CPP
DEP_CPP_DATAF=\
	"..\include\_debug.h"\
	"..\include\_error.h"\
	"..\include\_help.h"\
	"..\include\_msgs.h"\
	"..\include\_windwrd.h"\
	"..\include\blt.h"\
	"..\include\btree.h"\
	"..\include\cache.h"\
	"..\include\codec.h"\
	"..\include\datafile.h"\
	"..\include\dib.h"\
	"..\include\dibwnd.h"\
	"..\include\DlgMsg.h"\
	"..\include\fixpoint.h"\
	"..\include\games.h"\
	"..\include\init.h"\
	"..\include\logging.h"\
	"..\include\mmio.h"\
	"..\include\mmio.inl"\
	"..\include\ptr.h"\
	"..\include\rand.h"\
	"..\include\scanlist.h"\
	"..\include\subclass.h"\
	"..\include\thielen.h"\
	"..\include\thielen.inl"\
	"..\include\threads.h"\
	"..\include\windward.h"\
	"..\include\windward.inl"\
	"..\include\wndbase.h"\
	".\..\include\dave32ut.h"\
	".\..\include\gg.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

!IF  "$(CFG)" == "Windward - Win32 Release"


"$(INTDIR)\DATAFILE.OBJ" : $(SOURCE) $(DEP_CPP_DATAF) "$(INTDIR)"

"$(INTDIR)\DATAFILE.SBR" : $(SOURCE) $(DEP_CPP_DATAF) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"


"$(INTDIR)\DATAFILE.OBJ" : $(SOURCE) $(DEP_CPP_DATAF) "$(INTDIR)"

"$(INTDIR)\DATAFILE.SBR" : $(SOURCE) $(DEP_CPP_DATAF) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"


"$(INTDIR)\DATAFILE.OBJ" : $(SOURCE) $(DEP_CPP_DATAF) "$(INTDIR)"

"$(INTDIR)\DATAFILE.SBR" : $(SOURCE) $(DEP_CPP_DATAF) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"


"$(INTDIR)\DATAFILE.OBJ" : $(SOURCE) $(DEP_CPP_DATAF) "$(INTDIR)"

"$(INTDIR)\DATAFILE.SBR" : $(SOURCE) $(DEP_CPP_DATAF) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\FIXPOINT.CPP
DEP_CPP_FIXPO=\
	"..\include\_debug.h"\
	"..\include\_error.h"\
	"..\include\_help.h"\
	"..\include\_msgs.h"\
	"..\include\_windwrd.h"\
	"..\include\blt.h"\
	"..\include\btree.h"\
	"..\include\cache.h"\
	"..\include\codec.h"\
	"..\include\datafile.h"\
	"..\include\dib.h"\
	"..\include\dibwnd.h"\
	"..\include\DlgMsg.h"\
	"..\include\fixpoint.h"\
	"..\include\games.h"\
	"..\include\init.h"\
	"..\include\logging.h"\
	"..\include\mmio.h"\
	"..\include\mmio.inl"\
	"..\include\ptr.h"\
	"..\include\rand.h"\
	"..\include\scanlist.h"\
	"..\include\subclass.h"\
	"..\include\thielen.h"\
	"..\include\thielen.inl"\
	"..\include\threads.h"\
	"..\include\windward.h"\
	"..\include\windward.inl"\
	"..\include\wndbase.h"\
	".\..\include\dave32ut.h"\
	".\..\include\gg.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

!IF  "$(CFG)" == "Windward - Win32 Release"


"$(INTDIR)\FIXPOINT.OBJ" : $(SOURCE) $(DEP_CPP_FIXPO) "$(INTDIR)"

"$(INTDIR)\FIXPOINT.SBR" : $(SOURCE) $(DEP_CPP_FIXPO) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"


"$(INTDIR)\FIXPOINT.OBJ" : $(SOURCE) $(DEP_CPP_FIXPO) "$(INTDIR)"

"$(INTDIR)\FIXPOINT.SBR" : $(SOURCE) $(DEP_CPP_FIXPO) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"


"$(INTDIR)\FIXPOINT.OBJ" : $(SOURCE) $(DEP_CPP_FIXPO) "$(INTDIR)"

"$(INTDIR)\FIXPOINT.SBR" : $(SOURCE) $(DEP_CPP_FIXPO) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"


"$(INTDIR)\FIXPOINT.OBJ" : $(SOURCE) $(DEP_CPP_FIXPO) "$(INTDIR)"

"$(INTDIR)\FIXPOINT.SBR" : $(SOURCE) $(DEP_CPP_FIXPO) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\WINDWARD.RC

!IF  "$(CFG)" == "Windward - Win32 Release"

!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"

!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"

!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"

!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\DIBWND.CPP
DEP_CPP_DIBWN=\
	"..\include\_debug.h"\
	"..\include\_error.h"\
	"..\include\_help.h"\
	"..\include\_msgs.h"\
	"..\include\_windwrd.h"\
	"..\include\blt.h"\
	"..\include\btree.h"\
	"..\include\cache.h"\
	"..\include\codec.h"\
	"..\include\datafile.h"\
	"..\include\dib.h"\
	"..\include\dibwnd.h"\
	"..\include\DlgMsg.h"\
	"..\include\fixpoint.h"\
	"..\include\games.h"\
	"..\include\init.h"\
	"..\include\logging.h"\
	"..\include\mmio.h"\
	"..\include\mmio.inl"\
	"..\include\ptr.h"\
	"..\include\rand.h"\
	"..\include\scanlist.h"\
	"..\include\subclass.h"\
	"..\include\thielen.h"\
	"..\include\thielen.inl"\
	"..\include\threads.h"\
	"..\include\windward.h"\
	"..\include\windward.inl"\
	"..\include\wndbase.h"\
	".\..\include\dave32ut.h"\
	".\..\include\gg.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

!IF  "$(CFG)" == "Windward - Win32 Release"


"$(INTDIR)\DIBWND.OBJ" : $(SOURCE) $(DEP_CPP_DIBWN) "$(INTDIR)"

"$(INTDIR)\DIBWND.SBR" : $(SOURCE) $(DEP_CPP_DIBWN) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"


"$(INTDIR)\DIBWND.OBJ" : $(SOURCE) $(DEP_CPP_DIBWN) "$(INTDIR)"

"$(INTDIR)\DIBWND.SBR" : $(SOURCE) $(DEP_CPP_DIBWN) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"


"$(INTDIR)\DIBWND.OBJ" : $(SOURCE) $(DEP_CPP_DIBWN) "$(INTDIR)"

"$(INTDIR)\DIBWND.SBR" : $(SOURCE) $(DEP_CPP_DIBWN) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"


"$(INTDIR)\DIBWND.OBJ" : $(SOURCE) $(DEP_CPP_DIBWN) "$(INTDIR)"

"$(INTDIR)\DIBWND.SBR" : $(SOURCE) $(DEP_CPP_DIBWN) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\BLT.CPP
DEP_CPP_BLT_C=\
	"..\include\_debug.h"\
	"..\include\_error.h"\
	"..\include\_help.h"\
	"..\include\_msgs.h"\
	"..\include\_windwrd.h"\
	"..\include\blt.h"\
	"..\include\btree.h"\
	"..\include\cache.h"\
	"..\include\codec.h"\
	"..\include\datafile.h"\
	"..\include\dib.h"\
	"..\include\dibwnd.h"\
	"..\include\DlgMsg.h"\
	"..\include\fixpoint.h"\
	"..\include\games.h"\
	"..\include\init.h"\
	"..\include\logging.h"\
	"..\include\mmio.h"\
	"..\include\mmio.inl"\
	"..\include\ptr.h"\
	"..\include\rand.h"\
	"..\include\scanlist.h"\
	"..\include\subclass.h"\
	"..\include\thielen.h"\
	"..\include\thielen.inl"\
	"..\include\threads.h"\
	"..\include\windward.h"\
	"..\include\windward.inl"\
	"..\include\wndbase.h"\
	".\..\include\dave32ut.h"\
	".\..\include\gg.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

!IF  "$(CFG)" == "Windward - Win32 Release"


"$(INTDIR)\BLT.OBJ" : $(SOURCE) $(DEP_CPP_BLT_C) "$(INTDIR)"

"$(INTDIR)\BLT.SBR" : $(SOURCE) $(DEP_CPP_BLT_C) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"


"$(INTDIR)\BLT.OBJ" : $(SOURCE) $(DEP_CPP_BLT_C) "$(INTDIR)"

"$(INTDIR)\BLT.SBR" : $(SOURCE) $(DEP_CPP_BLT_C) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"


"$(INTDIR)\BLT.OBJ" : $(SOURCE) $(DEP_CPP_BLT_C) "$(INTDIR)"

"$(INTDIR)\BLT.SBR" : $(SOURCE) $(DEP_CPP_BLT_C) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"


"$(INTDIR)\BLT.OBJ" : $(SOURCE) $(DEP_CPP_BLT_C) "$(INTDIR)"

"$(INTDIR)\BLT.SBR" : $(SOURCE) $(DEP_CPP_BLT_C) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\DIB.CPP
DEP_CPP_DIB_C=\
	"..\include\_debug.h"\
	"..\include\_error.h"\
	"..\include\_help.h"\
	"..\include\_msgs.h"\
	"..\include\_windwrd.h"\
	"..\include\blt.h"\
	"..\include\btree.h"\
	"..\include\cache.h"\
	"..\include\codec.h"\
	"..\include\datafile.h"\
	"..\include\dib.h"\
	"..\include\dibwnd.h"\
	"..\include\DlgMsg.h"\
	"..\include\fixpoint.h"\
	"..\include\games.h"\
	"..\include\init.h"\
	"..\include\logging.h"\
	"..\include\mmio.h"\
	"..\include\mmio.inl"\
	"..\include\ptr.h"\
	"..\include\rand.h"\
	"..\include\scanlist.h"\
	"..\include\subclass.h"\
	"..\include\thielen.h"\
	"..\include\thielen.inl"\
	"..\include\threads.h"\
	"..\include\windward.h"\
	"..\include\windward.inl"\
	"..\include\wndbase.h"\
	".\..\include\dave32ut.h"\
	".\..\include\gg.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

!IF  "$(CFG)" == "Windward - Win32 Release"


"$(INTDIR)\DIB.OBJ" : $(SOURCE) $(DEP_CPP_DIB_C) "$(INTDIR)"

"$(INTDIR)\DIB.SBR" : $(SOURCE) $(DEP_CPP_DIB_C) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"


"$(INTDIR)\DIB.OBJ" : $(SOURCE) $(DEP_CPP_DIB_C) "$(INTDIR)"

"$(INTDIR)\DIB.SBR" : $(SOURCE) $(DEP_CPP_DIB_C) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"


"$(INTDIR)\DIB.OBJ" : $(SOURCE) $(DEP_CPP_DIB_C) "$(INTDIR)"

"$(INTDIR)\DIB.SBR" : $(SOURCE) $(DEP_CPP_DIB_C) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"


"$(INTDIR)\DIB.OBJ" : $(SOURCE) $(DEP_CPP_DIB_C) "$(INTDIR)"

"$(INTDIR)\DIB.SBR" : $(SOURCE) $(DEP_CPP_DIB_C) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\BTREE.CPP
DEP_CPP_BTREE=\
	"..\include\_debug.h"\
	"..\include\_error.h"\
	"..\include\_help.h"\
	"..\include\_msgs.h"\
	"..\include\_windwrd.h"\
	"..\include\blt.h"\
	"..\include\btree.h"\
	"..\include\cache.h"\
	"..\include\codec.h"\
	"..\include\datafile.h"\
	"..\include\dib.h"\
	"..\include\dibwnd.h"\
	"..\include\DlgMsg.h"\
	"..\include\fixpoint.h"\
	"..\include\games.h"\
	"..\include\init.h"\
	"..\include\logging.h"\
	"..\include\mmio.h"\
	"..\include\mmio.inl"\
	"..\include\ptr.h"\
	"..\include\rand.h"\
	"..\include\scanlist.h"\
	"..\include\subclass.h"\
	"..\include\thielen.h"\
	"..\include\thielen.inl"\
	"..\include\threads.h"\
	"..\include\windward.h"\
	"..\include\windward.inl"\
	"..\include\wndbase.h"\
	".\..\include\dave32ut.h"\
	".\..\include\gg.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

!IF  "$(CFG)" == "Windward - Win32 Release"


"$(INTDIR)\BTREE.OBJ" : $(SOURCE) $(DEP_CPP_BTREE) "$(INTDIR)"

"$(INTDIR)\BTREE.SBR" : $(SOURCE) $(DEP_CPP_BTREE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"


"$(INTDIR)\BTREE.OBJ" : $(SOURCE) $(DEP_CPP_BTREE) "$(INTDIR)"

"$(INTDIR)\BTREE.SBR" : $(SOURCE) $(DEP_CPP_BTREE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"


"$(INTDIR)\BTREE.OBJ" : $(SOURCE) $(DEP_CPP_BTREE) "$(INTDIR)"

"$(INTDIR)\BTREE.SBR" : $(SOURCE) $(DEP_CPP_BTREE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"


"$(INTDIR)\BTREE.OBJ" : $(SOURCE) $(DEP_CPP_BTREE) "$(INTDIR)"

"$(INTDIR)\BTREE.SBR" : $(SOURCE) $(DEP_CPP_BTREE) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\Flcanim.cpp
DEP_CPP_FLCAN=\
	"..\include\_debug.h"\
	"..\include\_error.h"\
	"..\include\_help.h"\
	"..\include\_msgs.h"\
	"..\include\_windwrd.h"\
	"..\include\blt.h"\
	"..\include\btree.h"\
	"..\include\cache.h"\
	"..\include\codec.h"\
	"..\include\datafile.h"\
	"..\include\dib.h"\
	"..\include\dibwnd.h"\
	"..\include\DlgMsg.h"\
	"..\include\fixpoint.h"\
	"..\include\flcanim.h"\
	"..\include\games.h"\
	"..\include\init.h"\
	"..\include\logging.h"\
	"..\include\mmio.h"\
	"..\include\mmio.inl"\
	"..\include\ptr.h"\
	"..\include\rand.h"\
	"..\include\scanlist.h"\
	"..\include\subclass.h"\
	"..\include\thielen.h"\
	"..\include\thielen.inl"\
	"..\include\threads.h"\
	"..\include\windward.h"\
	"..\include\windward.inl"\
	"..\include\wndbase.h"\
	".\..\include\dave32ut.h"\
	".\..\include\gg.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

!IF  "$(CFG)" == "Windward - Win32 Release"


"$(INTDIR)\Flcanim.obj" : $(SOURCE) $(DEP_CPP_FLCAN) "$(INTDIR)"

"$(INTDIR)\Flcanim.sbr" : $(SOURCE) $(DEP_CPP_FLCAN) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"


"$(INTDIR)\Flcanim.obj" : $(SOURCE) $(DEP_CPP_FLCAN) "$(INTDIR)"

"$(INTDIR)\Flcanim.sbr" : $(SOURCE) $(DEP_CPP_FLCAN) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"


"$(INTDIR)\Flcanim.obj" : $(SOURCE) $(DEP_CPP_FLCAN) "$(INTDIR)"

"$(INTDIR)\Flcanim.sbr" : $(SOURCE) $(DEP_CPP_FLCAN) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"


"$(INTDIR)\Flcanim.obj" : $(SOURCE) $(DEP_CPP_FLCAN) "$(INTDIR)"

"$(INTDIR)\Flcanim.sbr" : $(SOURCE) $(DEP_CPP_FLCAN) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\Flcctrl.cpp
DEP_CPP_FLCCT=\
	"..\include\_debug.h"\
	"..\include\_error.h"\
	"..\include\_help.h"\
	"..\include\_msgs.h"\
	"..\include\_windwrd.h"\
	"..\include\blt.h"\
	"..\include\btree.h"\
	"..\include\cache.h"\
	"..\include\codec.h"\
	"..\include\datafile.h"\
	"..\include\dib.h"\
	"..\include\dibwnd.h"\
	"..\include\DlgMsg.h"\
	"..\include\fixpoint.h"\
	"..\include\flcanim.h"\
	"..\include\flcctrl.h"\
	"..\include\games.h"\
	"..\include\init.h"\
	"..\include\logging.h"\
	"..\include\mmio.h"\
	"..\include\mmio.inl"\
	"..\include\ptr.h"\
	"..\include\rand.h"\
	"..\include\scanlist.h"\
	"..\include\subclass.h"\
	"..\include\thielen.h"\
	"..\include\thielen.inl"\
	"..\include\threads.h"\
	"..\include\windward.h"\
	"..\include\windward.inl"\
	"..\include\wndbase.h"\
	".\..\include\dave32ut.h"\
	".\..\include\gg.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

!IF  "$(CFG)" == "Windward - Win32 Release"


"$(INTDIR)\Flcctrl.obj" : $(SOURCE) $(DEP_CPP_FLCCT) "$(INTDIR)"

"$(INTDIR)\Flcctrl.sbr" : $(SOURCE) $(DEP_CPP_FLCCT) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"


"$(INTDIR)\Flcctrl.obj" : $(SOURCE) $(DEP_CPP_FLCCT) "$(INTDIR)"

"$(INTDIR)\Flcctrl.sbr" : $(SOURCE) $(DEP_CPP_FLCCT) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"


"$(INTDIR)\Flcctrl.obj" : $(SOURCE) $(DEP_CPP_FLCCT) "$(INTDIR)"

"$(INTDIR)\Flcctrl.sbr" : $(SOURCE) $(DEP_CPP_FLCCT) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"


"$(INTDIR)\Flcctrl.obj" : $(SOURCE) $(DEP_CPP_FLCCT) "$(INTDIR)"

"$(INTDIR)\Flcctrl.sbr" : $(SOURCE) $(DEP_CPP_FLCCT) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\wndbase.cpp
DEP_CPP_WNDBA=\
	"..\include\_debug.h"\
	"..\include\_error.h"\
	"..\include\_help.h"\
	"..\include\_msgs.h"\
	"..\include\_windwrd.h"\
	"..\include\blt.h"\
	"..\include\btree.h"\
	"..\include\cache.h"\
	"..\include\codec.h"\
	"..\include\datafile.h"\
	"..\include\dib.h"\
	"..\include\dibwnd.h"\
	"..\include\DlgMsg.h"\
	"..\include\fixpoint.h"\
	"..\include\games.h"\
	"..\include\init.h"\
	"..\include\logging.h"\
	"..\include\mmio.h"\
	"..\include\mmio.inl"\
	"..\include\ptr.h"\
	"..\include\rand.h"\
	"..\include\scanlist.h"\
	"..\include\subclass.h"\
	"..\include\thielen.h"\
	"..\include\thielen.inl"\
	"..\include\threads.h"\
	"..\include\windward.h"\
	"..\include\windward.inl"\
	"..\include\wndbase.h"\
	".\..\include\dave32ut.h"\
	".\..\include\gg.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

!IF  "$(CFG)" == "Windward - Win32 Release"


"$(INTDIR)\wndbase.obj" : $(SOURCE) $(DEP_CPP_WNDBA) "$(INTDIR)"

"$(INTDIR)\wndbase.sbr" : $(SOURCE) $(DEP_CPP_WNDBA) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"


"$(INTDIR)\wndbase.obj" : $(SOURCE) $(DEP_CPP_WNDBA) "$(INTDIR)"

"$(INTDIR)\wndbase.sbr" : $(SOURCE) $(DEP_CPP_WNDBA) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"


"$(INTDIR)\wndbase.obj" : $(SOURCE) $(DEP_CPP_WNDBA) "$(INTDIR)"

"$(INTDIR)\wndbase.sbr" : $(SOURCE) $(DEP_CPP_WNDBA) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"


"$(INTDIR)\wndbase.obj" : $(SOURCE) $(DEP_CPP_WNDBA) "$(INTDIR)"

"$(INTDIR)\wndbase.sbr" : $(SOURCE) $(DEP_CPP_WNDBA) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\Subclass.cpp
DEP_CPP_SUBCL=\
	"..\include\_debug.h"\
	"..\include\_error.h"\
	"..\include\_help.h"\
	"..\include\_msgs.h"\
	"..\include\_windwrd.h"\
	"..\include\acmutil.h"\
	"..\include\blt.h"\
	"..\include\btree.h"\
	"..\include\cache.h"\
	"..\include\codec.h"\
	"..\include\datafile.h"\
	"..\include\dib.h"\
	"..\include\dibwnd.h"\
	"..\include\DlgMsg.h"\
	"..\include\fixpoint.h"\
	"..\include\games.h"\
	"..\include\init.h"\
	"..\include\logging.h"\
	"..\include\mmio.h"\
	"..\include\mmio.inl"\
	"..\include\music.h"\
	"..\include\ptr.h"\
	"..\include\rand.h"\
	"..\include\scanlist.h"\
	"..\include\subclass.h"\
	"..\include\thielen.h"\
	"..\include\thielen.inl"\
	"..\include\threads.h"\
	"..\include\windward.h"\
	"..\include\windward.inl"\
	"..\include\wndbase.h"\
	".\..\include\dave32ut.h"\
	".\..\include\gg.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

!IF  "$(CFG)" == "Windward - Win32 Release"


"$(INTDIR)\Subclass.obj" : $(SOURCE) $(DEP_CPP_SUBCL) "$(INTDIR)"

"$(INTDIR)\Subclass.sbr" : $(SOURCE) $(DEP_CPP_SUBCL) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"


"$(INTDIR)\Subclass.obj" : $(SOURCE) $(DEP_CPP_SUBCL) "$(INTDIR)"

"$(INTDIR)\Subclass.sbr" : $(SOURCE) $(DEP_CPP_SUBCL) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"


"$(INTDIR)\Subclass.obj" : $(SOURCE) $(DEP_CPP_SUBCL) "$(INTDIR)"

"$(INTDIR)\Subclass.sbr" : $(SOURCE) $(DEP_CPP_SUBCL) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"


"$(INTDIR)\Subclass.obj" : $(SOURCE) $(DEP_CPP_SUBCL) "$(INTDIR)"

"$(INTDIR)\Subclass.sbr" : $(SOURCE) $(DEP_CPP_SUBCL) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\logging.CPP
DEP_CPP_LOGGI=\
	"..\include\_debug.h"\
	"..\include\_error.h"\
	"..\include\_help.h"\
	"..\include\_msgs.h"\
	"..\include\_windwrd.h"\
	"..\include\blt.h"\
	"..\include\btree.h"\
	"..\include\cache.h"\
	"..\include\codec.h"\
	"..\include\datafile.h"\
	"..\include\dib.h"\
	"..\include\dibwnd.h"\
	"..\include\DlgMsg.h"\
	"..\include\fixpoint.h"\
	"..\include\games.h"\
	"..\include\init.h"\
	"..\include\logging.h"\
	"..\include\mmio.h"\
	"..\include\mmio.inl"\
	"..\include\ptr.h"\
	"..\include\rand.h"\
	"..\include\scanlist.h"\
	"..\include\subclass.h"\
	"..\include\thielen.h"\
	"..\include\thielen.inl"\
	"..\include\threads.h"\
	"..\include\windward.h"\
	"..\include\windward.inl"\
	"..\include\wndbase.h"\
	".\..\include\dave32ut.h"\
	".\..\include\gg.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

!IF  "$(CFG)" == "Windward - Win32 Release"


"$(INTDIR)\logging.obj" : $(SOURCE) $(DEP_CPP_LOGGI) "$(INTDIR)"

"$(INTDIR)\logging.sbr" : $(SOURCE) $(DEP_CPP_LOGGI) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"


"$(INTDIR)\logging.obj" : $(SOURCE) $(DEP_CPP_LOGGI) "$(INTDIR)"

"$(INTDIR)\logging.sbr" : $(SOURCE) $(DEP_CPP_LOGGI) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"


"$(INTDIR)\logging.obj" : $(SOURCE) $(DEP_CPP_LOGGI) "$(INTDIR)"

"$(INTDIR)\logging.sbr" : $(SOURCE) $(DEP_CPP_LOGGI) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"


"$(INTDIR)\logging.obj" : $(SOURCE) $(DEP_CPP_LOGGI) "$(INTDIR)"

"$(INTDIR)\logging.sbr" : $(SOURCE) $(DEP_CPP_LOGGI) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\cache.cpp
DEP_CPP_CACHE=\
	"..\include\_debug.h"\
	"..\include\_error.h"\
	"..\include\_help.h"\
	"..\include\_msgs.h"\
	"..\include\_windwrd.h"\
	"..\include\blt.h"\
	"..\include\btree.h"\
	"..\include\cache.h"\
	"..\include\codec.h"\
	"..\include\datafile.h"\
	"..\include\dib.h"\
	"..\include\dibwnd.h"\
	"..\include\DlgMsg.h"\
	"..\include\fixpoint.h"\
	"..\include\games.h"\
	"..\include\init.h"\
	"..\include\logging.h"\
	"..\include\mmio.h"\
	"..\include\mmio.inl"\
	"..\include\ptr.h"\
	"..\include\rand.h"\
	"..\include\scanlist.h"\
	"..\include\subclass.h"\
	"..\include\thielen.h"\
	"..\include\thielen.inl"\
	"..\include\threads.h"\
	"..\include\windward.h"\
	"..\include\windward.inl"\
	"..\include\wndbase.h"\
	".\..\include\dave32ut.h"\
	".\..\include\gg.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

!IF  "$(CFG)" == "Windward - Win32 Release"


"$(INTDIR)\cache.obj" : $(SOURCE) $(DEP_CPP_CACHE) "$(INTDIR)"

"$(INTDIR)\cache.sbr" : $(SOURCE) $(DEP_CPP_CACHE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"


"$(INTDIR)\cache.obj" : $(SOURCE) $(DEP_CPP_CACHE) "$(INTDIR)"

"$(INTDIR)\cache.sbr" : $(SOURCE) $(DEP_CPP_CACHE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"


"$(INTDIR)\cache.obj" : $(SOURCE) $(DEP_CPP_CACHE) "$(INTDIR)"

"$(INTDIR)\cache.sbr" : $(SOURCE) $(DEP_CPP_CACHE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"


"$(INTDIR)\cache.obj" : $(SOURCE) $(DEP_CPP_CACHE) "$(INTDIR)"

"$(INTDIR)\cache.sbr" : $(SOURCE) $(DEP_CPP_CACHE) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\DlgMsg.cpp
DEP_CPP_DLGMS=\
	"..\include\_debug.h"\
	"..\include\_error.h"\
	"..\include\_help.h"\
	"..\include\_msgs.h"\
	"..\include\_windwrd.h"\
	"..\include\blt.h"\
	"..\include\btree.h"\
	"..\include\cache.h"\
	"..\include\codec.h"\
	"..\include\datafile.h"\
	"..\include\dib.h"\
	"..\include\dibwnd.h"\
	"..\include\DlgMsg.h"\
	"..\include\fixpoint.h"\
	"..\include\games.h"\
	"..\include\init.h"\
	"..\include\logging.h"\
	"..\include\mmio.h"\
	"..\include\mmio.inl"\
	"..\include\ptr.h"\
	"..\include\rand.h"\
	"..\include\scanlist.h"\
	"..\include\subclass.h"\
	"..\include\thielen.h"\
	"..\include\thielen.inl"\
	"..\include\threads.h"\
	"..\include\windward.h"\
	"..\include\windward.inl"\
	"..\include\wndbase.h"\
	".\..\include\dave32ut.h"\
	".\..\include\gg.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

!IF  "$(CFG)" == "Windward - Win32 Release"


"$(INTDIR)\DlgMsg.obj" : $(SOURCE) $(DEP_CPP_DLGMS) "$(INTDIR)"

"$(INTDIR)\DlgMsg.sbr" : $(SOURCE) $(DEP_CPP_DLGMS) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"


"$(INTDIR)\DlgMsg.obj" : $(SOURCE) $(DEP_CPP_DLGMS) "$(INTDIR)"

"$(INTDIR)\DlgMsg.sbr" : $(SOURCE) $(DEP_CPP_DLGMS) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"


"$(INTDIR)\DlgMsg.obj" : $(SOURCE) $(DEP_CPP_DLGMS) "$(INTDIR)"

"$(INTDIR)\DlgMsg.sbr" : $(SOURCE) $(DEP_CPP_DLGMS) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"


"$(INTDIR)\DlgMsg.obj" : $(SOURCE) $(DEP_CPP_DLGMS) "$(INTDIR)"

"$(INTDIR)\DlgMsg.sbr" : $(SOURCE) $(DEP_CPP_DLGMS) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\LZWCODEC.CPP

!IF  "$(CFG)" == "Windward - Win32 Release"

DEP_CPP_LZWCO=\
	"..\include\bitbuffe.h"\
	"..\include\codec.h"\
	"..\include\lzwcodec.h"\
	"..\include\thielen.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

"$(INTDIR)\LZWCODEC.OBJ" : $(SOURCE) $(DEP_CPP_LZWCO) "$(INTDIR)"

"$(INTDIR)\LZWCODEC.SBR" : $(SOURCE) $(DEP_CPP_LZWCO) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"

DEP_CPP_LZWCO=\
	"..\include\bitbuffe.h"\
	"..\include\codec.h"\
	"..\include\lzwcodec.h"\
	"..\include\thielen.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

"$(INTDIR)\LZWCODEC.OBJ" : $(SOURCE) $(DEP_CPP_LZWCO) "$(INTDIR)"

"$(INTDIR)\LZWCODEC.SBR" : $(SOURCE) $(DEP_CPP_LZWCO) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"

DEP_CPP_LZWCO=\
	"..\include\bitbuffe.h"\
	"..\include\codec.h"\
	"..\include\lzwcodec.h"\
	"..\include\thielen.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

"$(INTDIR)\LZWCODEC.OBJ" : $(SOURCE) $(DEP_CPP_LZWCO) "$(INTDIR)"

"$(INTDIR)\LZWCODEC.SBR" : $(SOURCE) $(DEP_CPP_LZWCO) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"

DEP_CPP_LZWCO=\
	"..\include\bitbuffe.h"\
	"..\include\codec.h"\
	"..\include\lzwcodec.h"\
	"..\include\thielen.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

"$(INTDIR)\LZWCODEC.OBJ" : $(SOURCE) $(DEP_CPP_LZWCO) "$(INTDIR)"

"$(INTDIR)\LZWCODEC.SBR" : $(SOURCE) $(DEP_CPP_LZWCO) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\LZSSCODE.CPP

!IF  "$(CFG)" == "Windward - Win32 Release"

DEP_CPP_LZSSC=\
	"..\include\bitbuffe.h"\
	"..\include\codec.h"\
	"..\include\lzsscode.h"\
	"..\include\thielen.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

"$(INTDIR)\LZSSCODE.OBJ" : $(SOURCE) $(DEP_CPP_LZSSC) "$(INTDIR)"

"$(INTDIR)\LZSSCODE.SBR" : $(SOURCE) $(DEP_CPP_LZSSC) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"

DEP_CPP_LZSSC=\
	"..\include\bitbuffe.h"\
	"..\include\codec.h"\
	"..\include\lzsscode.h"\
	"..\include\thielen.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

"$(INTDIR)\LZSSCODE.OBJ" : $(SOURCE) $(DEP_CPP_LZSSC) "$(INTDIR)"

"$(INTDIR)\LZSSCODE.SBR" : $(SOURCE) $(DEP_CPP_LZSSC) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"

DEP_CPP_LZSSC=\
	"..\include\bitbuffe.h"\
	"..\include\codec.h"\
	"..\include\lzsscode.h"\
	"..\include\thielen.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

"$(INTDIR)\LZSSCODE.OBJ" : $(SOURCE) $(DEP_CPP_LZSSC) "$(INTDIR)"

"$(INTDIR)\LZSSCODE.SBR" : $(SOURCE) $(DEP_CPP_LZSSC) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"

DEP_CPP_LZSSC=\
	"..\include\bitbuffe.h"\
	"..\include\codec.h"\
	"..\include\lzsscode.h"\
	"..\include\thielen.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

"$(INTDIR)\LZSSCODE.OBJ" : $(SOURCE) $(DEP_CPP_LZSSC) "$(INTDIR)"

"$(INTDIR)\LZSSCODE.SBR" : $(SOURCE) $(DEP_CPP_LZSSC) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\HUFFMANC.CPP

!IF  "$(CFG)" == "Windward - Win32 Release"

DEP_CPP_HUFFM=\
	"..\include\bitbuffe.h"\
	"..\include\codec.h"\
	"..\include\huffmanc.h"\
	"..\include\thielen.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

"$(INTDIR)\HUFFMANC.OBJ" : $(SOURCE) $(DEP_CPP_HUFFM) "$(INTDIR)"

"$(INTDIR)\HUFFMANC.SBR" : $(SOURCE) $(DEP_CPP_HUFFM) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"

DEP_CPP_HUFFM=\
	"..\include\bitbuffe.h"\
	"..\include\codec.h"\
	"..\include\huffmanc.h"\
	"..\include\thielen.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

"$(INTDIR)\HUFFMANC.OBJ" : $(SOURCE) $(DEP_CPP_HUFFM) "$(INTDIR)"

"$(INTDIR)\HUFFMANC.SBR" : $(SOURCE) $(DEP_CPP_HUFFM) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"

DEP_CPP_HUFFM=\
	"..\include\bitbuffe.h"\
	"..\include\codec.h"\
	"..\include\huffmanc.h"\
	"..\include\thielen.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

"$(INTDIR)\HUFFMANC.OBJ" : $(SOURCE) $(DEP_CPP_HUFFM) "$(INTDIR)"

"$(INTDIR)\HUFFMANC.SBR" : $(SOURCE) $(DEP_CPP_HUFFM) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"

DEP_CPP_HUFFM=\
	"..\include\bitbuffe.h"\
	"..\include\codec.h"\
	"..\include\huffmanc.h"\
	"..\include\thielen.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

"$(INTDIR)\HUFFMANC.OBJ" : $(SOURCE) $(DEP_CPP_HUFFM) "$(INTDIR)"

"$(INTDIR)\HUFFMANC.SBR" : $(SOURCE) $(DEP_CPP_HUFFM) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\codec.cpp
DEP_CPP_CODEC=\
	"..\include\_debug.h"\
	"..\include\_error.h"\
	"..\include\_help.h"\
	"..\include\_msgs.h"\
	"..\include\_windwrd.h"\
	"..\include\bitbuffe.h"\
	"..\include\blt.h"\
	"..\include\bpecodec.h"\
	"..\include\btree.h"\
	"..\include\cache.h"\
	"..\include\codec.h"\
	"..\include\datafile.h"\
	"..\include\dib.h"\
	"..\include\dibwnd.h"\
	"..\include\DlgMsg.h"\
	"..\include\fixpoint.h"\
	"..\include\games.h"\
	"..\include\huffmanc.h"\
	"..\include\init.h"\
	"..\include\logging.h"\
	"..\include\lzsscode.h"\
	"..\include\lzwcodec.h"\
	"..\include\mmio.h"\
	"..\include\mmio.inl"\
	"..\include\ptr.h"\
	"..\include\rand.h"\
	"..\include\scanlist.h"\
	"..\include\subclass.h"\
	"..\include\thielen.h"\
	"..\include\thielen.inl"\
	"..\include\threads.h"\
	"..\include\windward.h"\
	"..\include\windward.inl"\
	"..\include\wndbase.h"\
	".\..\include\dave32ut.h"\
	".\..\include\gg.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

!IF  "$(CFG)" == "Windward - Win32 Release"


"$(INTDIR)\codec.obj" : $(SOURCE) $(DEP_CPP_CODEC) "$(INTDIR)"

"$(INTDIR)\codec.sbr" : $(SOURCE) $(DEP_CPP_CODEC) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"


"$(INTDIR)\codec.obj" : $(SOURCE) $(DEP_CPP_CODEC) "$(INTDIR)"

"$(INTDIR)\codec.sbr" : $(SOURCE) $(DEP_CPP_CODEC) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"


"$(INTDIR)\codec.obj" : $(SOURCE) $(DEP_CPP_CODEC) "$(INTDIR)"

"$(INTDIR)\codec.sbr" : $(SOURCE) $(DEP_CPP_CODEC) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"


"$(INTDIR)\codec.obj" : $(SOURCE) $(DEP_CPP_CODEC) "$(INTDIR)"

"$(INTDIR)\codec.sbr" : $(SOURCE) $(DEP_CPP_CODEC) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\BPECODEC.CPP
DEP_CPP_BPECO=\
	"..\include\_debug.h"\
	"..\include\_error.h"\
	"..\include\_help.h"\
	"..\include\_msgs.h"\
	"..\include\_windwrd.h"\
	"..\include\blt.h"\
	"..\include\bpecodec.h"\
	"..\include\btree.h"\
	"..\include\cache.h"\
	"..\include\codec.h"\
	"..\include\datafile.h"\
	"..\include\dib.h"\
	"..\include\dibwnd.h"\
	"..\include\DlgMsg.h"\
	"..\include\fixpoint.h"\
	"..\include\games.h"\
	"..\include\init.h"\
	"..\include\logging.h"\
	"..\include\mmio.h"\
	"..\include\mmio.inl"\
	"..\include\ptr.h"\
	"..\include\rand.h"\
	"..\include\scanlist.h"\
	"..\include\subclass.h"\
	"..\include\thielen.h"\
	"..\include\thielen.inl"\
	"..\include\threads.h"\
	"..\include\windward.h"\
	"..\include\windward.inl"\
	"..\include\wndbase.h"\
	".\..\include\dave32ut.h"\
	".\..\include\gg.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

!IF  "$(CFG)" == "Windward - Win32 Release"


"$(INTDIR)\BPECODEC.OBJ" : $(SOURCE) $(DEP_CPP_BPECO) "$(INTDIR)"

"$(INTDIR)\BPECODEC.SBR" : $(SOURCE) $(DEP_CPP_BPECO) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"


"$(INTDIR)\BPECODEC.OBJ" : $(SOURCE) $(DEP_CPP_BPECO) "$(INTDIR)"

"$(INTDIR)\BPECODEC.SBR" : $(SOURCE) $(DEP_CPP_BPECO) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"


"$(INTDIR)\BPECODEC.OBJ" : $(SOURCE) $(DEP_CPP_BPECO) "$(INTDIR)"

"$(INTDIR)\BPECODEC.SBR" : $(SOURCE) $(DEP_CPP_BPECO) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"


"$(INTDIR)\BPECODEC.OBJ" : $(SOURCE) $(DEP_CPP_BPECO) "$(INTDIR)"

"$(INTDIR)\BPECODEC.SBR" : $(SOURCE) $(DEP_CPP_BPECO) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\BITBUFFE.CPP

!IF  "$(CFG)" == "Windward - Win32 Release"

DEP_CPP_BITBU=\
	"..\include\bitbuffe.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

"$(INTDIR)\BITBUFFE.OBJ" : $(SOURCE) $(DEP_CPP_BITBU) "$(INTDIR)"

"$(INTDIR)\BITBUFFE.SBR" : $(SOURCE) $(DEP_CPP_BITBU) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"

DEP_CPP_BITBU=\
	"..\include\bitbuffe.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

"$(INTDIR)\BITBUFFE.OBJ" : $(SOURCE) $(DEP_CPP_BITBU) "$(INTDIR)"

"$(INTDIR)\BITBUFFE.SBR" : $(SOURCE) $(DEP_CPP_BITBU) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"

DEP_CPP_BITBU=\
	"..\include\bitbuffe.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

"$(INTDIR)\BITBUFFE.OBJ" : $(SOURCE) $(DEP_CPP_BITBU) "$(INTDIR)"

"$(INTDIR)\BITBUFFE.SBR" : $(SOURCE) $(DEP_CPP_BITBU) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"

DEP_CPP_BITBU=\
	"..\include\bitbuffe.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

"$(INTDIR)\BITBUFFE.OBJ" : $(SOURCE) $(DEP_CPP_BITBU) "$(INTDIR)"

"$(INTDIR)\BITBUFFE.SBR" : $(SOURCE) $(DEP_CPP_BITBU) "$(INTDIR)"


!ENDIF 

# End Source File
################################################################################
# Begin Source File

SOURCE=.\ACMUTIL.CPP
DEP_CPP_ACMUT=\
	"..\include\_debug.h"\
	"..\include\_error.h"\
	"..\include\_help.h"\
	"..\include\_msgs.h"\
	"..\include\_windwrd.h"\
	"..\include\acmutil.h"\
	"..\include\blt.h"\
	"..\include\btree.h"\
	"..\include\cache.h"\
	"..\include\codec.h"\
	"..\include\datafile.h"\
	"..\include\dib.h"\
	"..\include\dibwnd.h"\
	"..\include\DlgMsg.h"\
	"..\include\fixpoint.h"\
	"..\include\games.h"\
	"..\include\init.h"\
	"..\include\logging.h"\
	"..\include\mmio.h"\
	"..\include\mmio.inl"\
	"..\include\music.h"\
	"..\include\ptr.h"\
	"..\include\rand.h"\
	"..\include\scanlist.h"\
	"..\include\subclass.h"\
	"..\include\thielen.h"\
	"..\include\thielen.inl"\
	"..\include\threads.h"\
	"..\include\windward.h"\
	"..\include\windward.inl"\
	"..\include\wndbase.h"\
	".\..\include\dave32ut.h"\
	".\..\include\gg.h"\
	".\STDAFX.H"\
	{$(INCLUDE)}"\MSSW.H"\
	{$(INCLUDE)}"\WING.H"\
	

!IF  "$(CFG)" == "Windward - Win32 Release"


"$(INTDIR)\ACMUTIL.OBJ" : $(SOURCE) $(DEP_CPP_ACMUT) "$(INTDIR)"

"$(INTDIR)\ACMUTIL.SBR" : $(SOURCE) $(DEP_CPP_ACMUT) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug"


"$(INTDIR)\ACMUTIL.OBJ" : $(SOURCE) $(DEP_CPP_ACMUT) "$(INTDIR)"

"$(INTDIR)\ACMUTIL.SBR" : $(SOURCE) $(DEP_CPP_ACMUT) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Release_dll"


"$(INTDIR)\ACMUTIL.OBJ" : $(SOURCE) $(DEP_CPP_ACMUT) "$(INTDIR)"

"$(INTDIR)\ACMUTIL.SBR" : $(SOURCE) $(DEP_CPP_ACMUT) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "Windward - Win32 Debug_dll"


"$(INTDIR)\ACMUTIL.OBJ" : $(SOURCE) $(DEP_CPP_ACMUT) "$(INTDIR)"

"$(INTDIR)\ACMUTIL.SBR" : $(SOURCE) $(DEP_CPP_ACMUT) "$(INTDIR)"


!ENDIF 

# End Source File
# End Target
# End Project
################################################################################
