This is the repo for the 1997 RTS enemy nations project update

Summary:
This is the source code for the 1997 RTS game enemy nations, released in 2009, and this repo was created as, yet another, attempt to get the source code to compile and run on modern day systems.


The Goal:
The (Long-term) goal of this project is to eventually, potentionally, get this project rewritten onto a new, basic, engine customised to this game.
But the goal of this project, in the short-term, is to get the original source code fixed, compiling, and running, at least without crashes, on a modern day system


The Plan:

Stage/Phase 1: 
The initial stage, of this project, is to currently update the project to be used with Visual Studio 2017 (Although vs 2010 may have been a better, since the C++ standard has changed quiet a bit since then, option but I couldn't find a copy to download) and fix the code to the point of, at least, getting it to compile and run without crashes (At the minimum) on a modern day OS (Windows 7).

Stage/Phase 2: 
Once the previous stage has been done then begin the next stage, of the project, by going through the source code and attempt to clean up and update, as well as improve, the code (to be more on par with modern day standards) and of course remove the heavy reliance of MFC (Microsoft Foundation Classes).

Stage/Phase 3:
Now the next stage of the plan would depend on whether the previous stage was/is successful/feasable:

If so, then finish up any code updates/improvements to the project and then potentionally release the resulting build to the public.

If not, then the next logical course would be to create a basic engine tailored to this particular game (Using a framework like SDL or SFML) and try to transfer the game code, from the original project,
to the new engine project and then adapt and change the code to fit the new engine, which will of course take a long time to do, given the manpower and skill available.

otherwise, should that fail, then a complete rewrite of the game will be the last resort/worst case to take if this game is to be revived (This is the ultimate last resort asides from abandoning the project altogether)


Other Info:
- Most of the useful/important info about the code will be commented in the code/project.
- I was able to get this project to compile and run, With a bit of help of course, (But did crash) on VS 2015. I still have that project if you are interested.
- The project contains a libraries that are very old/outdated and compiled in 16-bit which means that they may not function at all.
- There are 6 folders in this project, If you wish to know what their contents/purpose are then please read below:

  enations - The source code to the project (As far as I'm aware this could be the code from the released version) (This project is being worked on)
  
  enations_latest - The latest version of the source code as it was (According to the license 
  document) (This project is VS 2017)

  tools - Contains the libs used in the project (Including an old version of Miles Sound System)

  windward - The engine on which the game was built on (This project is VS 2107)

  windward_complete - Another version of the engine retreieved from a movie script writing program (Cannot remeber where from), this project in comparison with windward there appears to be a difference and perhaps more complete than the other one. (This project is not being worked on)


Credits:
Windward - For creating this awesome game and releasing the source code

And a special thanks to:
Adam Clarke,
Matthew Dodd,
and Jared Mulconry
for helping to fix the project


Finally:
If you have any questions or suggestions or if you wish to help on this project 
then please do not hesitate email me at: goncharuk_alexi@hotmail.com