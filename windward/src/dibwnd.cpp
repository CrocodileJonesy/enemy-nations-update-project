//---------------------------------------------------------------------------
//
//	Copyright (c) 1995, 1996. Windward Studios, Inc.
//	All Rights Reserved.
//
//---------------------------------------------------------------------------

#include "stdafx.h"
#include "_windwrd.h"
#include "dibwnd.h"

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif
#define new DEBUG_NEW

//--------------------------- C D i r t y R e c t s -----------------------------

const int MAX_DIRTY_RECTS = 200;

//---------------------------------------------------------------------------
// CDirtyRects::CDirtyRects
//---------------------------------------------------------------------------
CDirtyRects::CDirtyRects(
	CDIBWnd * pdibwnd )
	:
		m_pdibwnd  		 ( pdibwnd ),
		m_nRectPaintCur ( 0 ),
		m_nRectPaintNext( 0 ),
		m_nRectBlt 		 ( 0 ),
		m_prectPaintCur ( NULL ),
		m_prectPaintNext( NULL ),
		m_prectBlt 		 ( NULL )
{
	m_prectPaintCur  = new CRect [ MAX_DIRTY_RECTS ];
	m_prectPaintNext = new CRect [ MAX_DIRTY_RECTS ];
	m_prectBlt		  = new CRect [ MAX_DIRTY_RECTS ];
}

//---------------------------------------------------------------------------
// CDirtyRects::~CDirtyRects
//---------------------------------------------------------------------------
CDirtyRects::~CDirtyRects()
{
	delete [] m_prectPaintCur;
	delete [] m_prectPaintNext;
	delete [] m_prectBlt;
}

//---------------------------------------------------------------------------
// CDirtyRects::UpdateLists
//---------------------------------------------------------------------------
void
CDirtyRects::UpdateLists()
{
	ASSERT_VALID( this );

	m_nRectPaintCur  = m_nRectPaintNext;
	m_nRectPaintNext = 0;

	CRect * m_prectTemp = m_prectPaintCur;

	m_prectPaintCur  = m_prectPaintNext;
	m_prectPaintNext = m_prectTemp;
}

//---------------------------------------------------------------------------
// CDirtyRects::BltRects
//---------------------------------------------------------------------------
void
CDirtyRects::BltRects()
{
	ASSERT_VALID( this );

	for (	int i = 0; i < m_nRectBlt; ++i )
		if ( m_prectBlt[ i ].left != INT_MAX )
			m_pdibwnd->Paint( m_prectBlt + i );

	m_nRectBlt = 0;
}

//---------------------------------------------------------------------------
// CDirtyRects::AddRect
//---------------------------------------------------------------------------
void
CDirtyRects::AddRect(
	CRect const 			 * prect,
	CDirtyRects::RECT_LIST  eList )
{
	switch ( eList )
	{
		case LIST_PAINT_NEXT:	AddRect( prect, m_prectPaintNext, m_nRectPaintNext ); break;
		case LIST_PAINT_CUR:		AddRect( prect, m_prectPaintCur,  m_nRectPaintCur  ); break;
		case LIST_PAINT_BOTH:	AddRect( prect, m_prectPaintCur,  m_nRectPaintCur  );
										AddRect( prect, m_prectPaintNext, m_nRectPaintNext ); break;
		case LIST_BLT:				AddRect( prect, m_prectBlt,       m_nRectBlt       ); break;

		default: ASSERT( 0 );
	}
}

//---------------------------------------------------------------------------
// CDirtyRects::AddRect
//---------------------------------------------------------------------------
void
CDirtyRects::AddRect(
	CRect const * prect,
	CRect			  arect[],
	int			& nRect )
{
	ASSERT_VALID( this );

	CRect	rect;

	if ( !prect || nRect == MAX_DIRTY_RECTS )
	{
		//--------------------------------------------------------
		// if full-window, empty the list and add the client rect
		//--------------------------------------------------------

		nRect = 1;

		arect[ 0 ] = m_pdibwnd->GetRect();
	}
	else
	{
		int	iIndex = nRect;

		rect = *prect & m_pdibwnd->GetRect();

		if ( rect.IsRectEmpty() )
			return;

		for (	int i = 0; i < nRect; ++i )
		{
			CRect	& rectCur = arect[ i ];

			// Empty slot?

			if ( rectCur.left == INT_MAX )
			{
				iIndex = i;	// Reuseable slot

				continue;
			}

			// If the rects intersect, replace with the union

			BOOL	bIntersect = rect.left   < rectCur.right  &&
									 rect.right  > rectCur.left   &&
									 rect.top    < rectCur.bottom &&
									 rect.bottom > rectCur.top;

			if ( bIntersect )
			{
				rect |= rectCur;

				rectCur.left = INT_MAX;

				iIndex = i;	// Reusable slot
				i      = 0;	// Search from the start for more intersections
			}
		}

		if ( iIndex == nRect )	// Appending, bump # rects
			nRect++;

		arect[ iIndex ] = rect;
	}
}

//---------------------------------------------------------------------------
// CDirtyRects::AssertValid
//---------------------------------------------------------------------------
#ifdef _DEBUG
void CDirtyRects::AssertValid () const
{
	CObject::AssertValid();

	ASSERT_VALID( m_pdibwnd );
	ASSERT( AfxIsValidAddress( m_prectPaintCur,  MAX_DIRTY_RECTS * sizeof( m_prectPaintCur[0] )));
	ASSERT( AfxIsValidAddress( m_prectPaintNext, MAX_DIRTY_RECTS * sizeof( m_prectPaintCur[0] )));
	ASSERT( AfxIsValidAddress( m_prectBlt,       MAX_DIRTY_RECTS * sizeof( m_prectBlt[0]      )));
	ASSERT( 0 <= m_nRectPaintCur  && m_nRectPaintCur  < MAX_DIRTY_RECTS );
	ASSERT( 0 <= m_nRectPaintNext && m_nRectPaintNext < MAX_DIRTY_RECTS );
	ASSERT( 0 <= m_nRectBlt       && m_nRectBlt       < MAX_DIRTY_RECTS );
	ASSERT( m_prectPaintCur != m_prectPaintNext );
}
#endif

//----------------------------- C D I B W n d -----------------------------

//---------------------------------------------------------------------------
// CDIBWnd::ctor
//---------------------------------------------------------------------------
void 
CDIBWnd::ctor()
{
	m_hWnd    = NULL;
	m_hDC     = NULL;
	m_iWinWid = 1;
	m_iWinHt  = 1;

	m_pddclipper = NULL;
}

//---------------------------------------------------------------------------
// CDIBWnd::Init
//---------------------------------------------------------------------------
BOOL
CDIBWnd::Init(
	HWND 		  			  hWnd,
	Ptr< CDIB > const & ptrdib,			// new CDIB( ... )
	int 					  cx,
	int 					  cy )
{
	ASSERT_STRICT_VALID( this );

	m_hWnd 	= hWnd;
	m_ptrdib = ptrdib;

#ifdef _TRAP
	if ( (GetClassLong ( hWnd, GCL_STYLE ) & CS_OWNDC) == 0 )
		TRAP ();
#endif

	if (( m_hDC = GetDC( hWnd )) == NULL )
		return FALSE;

	// if it's a fixed size create the bitmap now

	if ( cx != 0 && cy != 0 )
		Size( cx, cy );

	if ( CBLTFormat::DIB_DIRECTDRAW == ptrdib->GetType() )
		if ( !CDirectDraw::GetTheDirectDraw()->GetDD()->CreateClipper( 0, &m_pddclipper, NULL ) && m_pddclipper )
			m_pddclipper->SetHWnd( 0, m_hWnd );

	ASSERT_STRICT_VALID( this );

	return TRUE;
}

//---------------------------------------------------------------------------
// CDIBWnd::Exit
//---------------------------------------------------------------------------
void CDIBWnd::Exit()
{
	if ( m_hDC )
		ReleaseDC( m_hWnd, m_hDC );

	if ( m_pddclipper )
		m_pddclipper->Release();

	ctor();
}

//---------------------------------------------------------------------------
// CDIBWnd::Size
//---------------------------------------------------------------------------
BOOL
CDIBWnd::Size(
	LPARAM lParam )
{
	ASSERT_STRICT_VALID( this );

	return Size(( int )LOWORD( lParam ),
		  			( int )HIWORD( lParam ));
}

//---------------------------------------------------------------------------
// CDIBWnd::Size
//---------------------------------------------------------------------------
BOOL
CDIBWnd::Size(
	int				cx,
	int				cy )
{
	ASSERT_STRICT_VALID( this );

	m_iWinWid = Max( 1, cx );
	m_iWinHt  = Max( 1, cy );

	return GetDIB()->Resize( m_iWinWid, m_iWinHt );
}

//---------------------------------------------------------------------------
// CDIBWnd::Paint
//---------------------------------------------------------------------------
void
CDIBWnd::Paint(
	CRect rect )
{
	ASSERT_STRICT_VALID( this );

	thePal.Paint( m_hDC );

	switch ( GetDIB()->GetType() )
	{
		case CBLTFormat::DIB_DIRECTDRAW:
		{
			CPoint	pt	= CPoint( rect.left, rect.top );

			::ClientToScreen( m_hWnd, &pt );

			m_hRes = ptrtheDirectDraw->GetFrontSurface()->SetClipper( m_pddclipper );

			CRect	rectDst( pt.x, pt.y, pt.x + rect.Width(), pt.y + rect.Height() );

			m_hRes = CDirectDraw::GetTheDirectDraw()->
				GetFrontSurface()->Blt(( LPRECT )&rectDst, GetDIB()->GetDDSurface(), ( LPRECT )&rect, DDBLT_WAIT, NULL );

			m_hRes = ptrtheDirectDraw->GetFrontSurface()->SetClipper( NULL );

			break;
		}

		default:

			GetDIB()->BitBlt( m_hDC, rect, rect.TopLeft() );
	}
}

//---------------------------------------------------------------------------
// CDIBWnd::Invalidate
//---------------------------------------------------------------------------
void
CDIBWnd::Invalidate(
	RECT const *pRect ) const
{
	ASSERT_STRICT_VALID( this );

	CRect	rect;

	if ( pRect )
	{
		rect = *pRect;

		rect.left   = Max( rect.left,   0L );
		rect.top    = Max( rect.top,    0L );
		rect.right  = Min( rect.right,  ( long )m_iWinWid );
		rect.bottom = Min( rect.bottom, ( long )m_iWinHt  );
	}
	else
		rect.SetRect( 0, 0, m_iWinWid, m_iWinHt );

	::InvalidateRect( m_hWnd, &rect, FALSE );
}

//---------------------------------------------------------------------------
// CDIBWnd::Invalidate
//---------------------------------------------------------------------------
void
CDIBWnd::Invalidate(
	int iLeft,
	int iTop,
	int iRight,
	int iBottom ) const
{
	ASSERT_STRICT_VALID( this );

	CRect rect( iLeft, iTop, iRight, iBottom );

	rect.left   = Max( rect.left,   0L );
	rect.top    = Max( rect.top,    0L );
	rect.right  = Min( rect.right,  ( long )m_iWinWid );
	rect.bottom = Min( rect.bottom, ( long )m_iWinHt  );

	::InvalidateRect( m_hWnd, &rect, FALSE );
}

//---------------------------------------------------------------------------
// CDIBWnd::Update
//---------------------------------------------------------------------------
void
CDIBWnd::Update () const
{
	ASSERT_STRICT_VALID( this );

	UpdateWindow( m_hWnd );
}

//---------------------------------------------------------------------------
// CDIBWnd::AssertValid
//---------------------------------------------------------------------------
#ifdef _DEBUG
void CDIBWnd::AssertValid () const
{
	CObject::AssertValid();

	if ( m_hWnd == NULL )
		return;

	ASSERT_STRICT( ::IsWindow( m_hWnd ));
	ASSERT_STRICT_VALID_OR_NULL( m_ptrdib.Value() );
}
#endif


