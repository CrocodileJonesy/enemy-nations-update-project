//--------------------------------------------------------------------------
//	flcanim.cpp		CFlcAnim class
//
// Copyright (c) 1994-1996 ChromeOcean Software - All rights reserved
// Reuse permission granted to Dave Thielen
//
//--------------------------------------------------------------------------

#include "stdafx.h"
#include "_windwrd.h"
#include "flcanim.h"
#include "dibwnd.h"


#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif
#define new DEBUG_NEW

#pragma intrinsic( memset )
#pragma intrinsic( memcpy )

const unsigned short FLC_TYPE = 0xaf12;

//--------------------------- C F l c H e a d e r --------------------------

#ifdef _DEBUG

BOOL
CFlcHeader::AssertValid() const
{
	ASSERT( 128 == sizeof( *this ));
	ASSERT( 0 < m_iSize );
	ASSERT( 0xaf12 == m_wType );
	ASSERT( 4000 >= m_nFrames );
	ASSERT( 0 < m_iWidth  && m_iWidth  <= 1280 );
	ASSERT( 0 < m_iHeight && m_iHeight <= 1024 );
	ASSERT( 8 == m_iBitsPerPixel );
	ASSERT( 0 <= m_iSpeed );

	return TRUE;
}

#endif

//------------------------ C F l c F r a m e H e a d e r --------------------

#ifdef _DEBUG

BOOL
CFlcFrameHeader::AssertValid() const
{
	ASSERT( m_iSize > 0 );
	ASSERT( 0xf1fa == m_wType );
	ASSERT( 0 <= m_nChunks && m_nChunks < 1000 );

	return TRUE;
}

#endif

//----------------------------- C F l c F r a m e --------------------------

//--------------------------------------------------------------
// CFlcFrame::CFlcFrame
//--------------------------------------------------------------
CFlcFrame::CFlcFrame(
	int 						iWidth,
	int 						iHeight,
	Ptr< CFile > const & ptrfile )
	:
		m_pbyBuffer   ( NULL ),
		m_pbyBufferEnd( NULL ),
		m_pbyBufferMax( NULL ),
		m_iMaxBuffer  ( 0 ),
		m_ptrfile     ( ptrfile ),
		m_iFrame		  ( -1 )
{
	ASSERT_VALID( ptrfile.Value() );
	ASSERT( iWidth > 0 && iHeight > 0 );

	m_iMaxBuffer = iWidth * iHeight +
					   3 * 256 + sizeof( WORD ) + 
						2 * sizeof( CFlcChunkHeader ) + 100; // fudge factor

	m_pbyBuffer 	= new BYTE [ m_iMaxBuffer ];
	m_pbyBufferEnd = m_pbyBuffer + 1;
	m_pbyBufferMax = m_pbyBuffer + m_iMaxBuffer;

	ASSERT_VALID( this );
}

//--------------------------------------------------------------
// CFlcFrame::~CFlcFrame
//--------------------------------------------------------------
CFlcFrame::~CFlcFrame()
{
	delete [] m_pbyBuffer;
}

//--------------------------------------------------------------
// CFlcFrame::LoadNextFrame
//--------------------------------------------------------------
void
CFlcFrame::LoadNextFrame()
{
	ASSERT_VALID( this );

	if ( sizeof( m_flcframehdr ) != m_ptrfile->Read( &m_flcframehdr, sizeof( m_flcframehdr )))
		ThrowError( ERR_FLC_READ_FRAME_HDR );

	m_flcframehdr.m_iSize -= sizeof( CFlcFrameHeader );

	if ( m_flcframehdr.GetSize() > m_iMaxBuffer )
		ThrowError( ERR_FLC_FRAME_HDR_SIZE );

	if ( m_flcframehdr.GetSize() != ( int )m_ptrfile->Read( m_pbyBuffer, m_flcframehdr.GetSize() ))
		ThrowError( ERR_FLC_READ_FRAME );

	m_pbyBufferEnd = m_pbyBuffer + m_flcframehdr.m_iSize;

	++m_iFrame;

	ASSERT_VALID( this );
}

//--------------------------------------------------------------
// CFlcFrame::AssertValid
//--------------------------------------------------------------
#ifdef _DEBUG
void
CFlcFrame::AssertValid() const
{
	CObject::AssertValid();

	ASSERT( 0 <= m_iMaxBuffer );
	ASSERT( AfxIsValidAddress( m_pbyBuffer, m_iMaxBuffer ));
	ASSERT( m_iMaxBuffer == m_pbyBufferMax - m_pbyBuffer );
	ASSERT( m_pbyBuffer <= m_pbyBufferEnd && m_pbyBufferEnd <= m_pbyBufferMax );
	ASSERT( m_iFrame >= -1 );
	ASSERT_VALID ( m_ptrfile.Value() );

	if ( 0 < m_iFrame )
		m_flcframehdr.AssertValid();
}

#endif

//----------------------- C F l c C h u n k H e a d e r --------------------

//--------------------------------------------------------------
// CFlcChunkHeader::AssertValid
//--------------------------------------------------------------
#ifdef _DEBUG
BOOL
CFlcChunkHeader::AssertValid() const
{
	ASSERT( 0 < m_iSize );
	ASSERT( 	COLOR256_CHUNK == m_wType ||
				SS2_CHUNK      == m_wType ||
				COLOR_CHUNK    == m_wType ||
				LC_CHUNK			== m_wType ||
				BLACK_CHUNK		== m_wType ||
				BRUN_CHUNK		== m_wType ||
				COPY_CHUNK		== m_wType ||
				PSTAMP_CHUNK 	== m_wType );

	return TRUE;
}
#endif

//------------------------------ C F l c A n i m ---------------------------

//--------------------------------------------------------------
// CFlcAnim::CFlcAnim
//--------------------------------------------------------------
CFlcAnim::CFlcAnim(
	Ptr< CFile > const & ptrfile )
	:
		m_pdibwnd		 ( NULL  ),
		m_pby    		 ( NULL  ),
		m_pdib			 ( NULL  ),
		m_prgbquad		 ( NULL  ),
		m_bInitialized  ( FALSE ),
		m_bIgnorePalette( FALSE )

{
	ASSERT_VALID( ptrfile.Value() );

	m_colorformat.CalcScreenFormat();

	if ( sizeof( m_flcheader ) != ptrfile->Read( &m_flcheader, sizeof( m_flcheader )))
		ThrowError( ERR_FLC_READ_HDR );
	else if ( FLC_TYPE != m_flcheader.GetType() )
		ThrowError( ERR_FLC_TYPE );

	m_ptrflcframe = new CFlcFrame( GetWidth(), GetHeight(), ptrfile );
	m_prgbquad 	  = new RGBQUAD [ 256 ];

	memset( m_prgbquad, 0, 256 * sizeof( RGBQUAD ));

	ASSERT_VALID( this );
}

//--------------------------------------------------------------
// CFlcAnim::~CFlcAnim
//--------------------------------------------------------------
CFlcAnim::~CFlcAnim()
{
	delete [] m_prgbquad;
}

//--------------------------------------------------------------
// CFlcAnim::Init
//--------------------------------------------------------------
void
CFlcAnim::Init(
	CDIBWnd * pdibwnd )
{
	ASSERT_VALID( this );
	ASSERT_VALID( pdibwnd );
	ASSERT( !IsInitialized() );

	m_bInitialized = TRUE;
	m_pdibwnd 		= pdibwnd;
	m_pdib 			= m_pdibwnd->GetDIB();

	ASSERT( 8 == m_pdib->GetBitsPerPixel() );

	if ( 8 != m_pdib->GetBitsPerPixel() )
		ThrowError( ERR_FLC_COLOR_DEPTH );
}

//--------------------------------------------------------------
// CFlcAnim::GetSpeed
//--------------------------------------------------------------
int
CFlcAnim::GetSpeed() const
{
	ASSERT_VALID( this );
	ASSERT( IsInitialized() );

	return m_flcheader.GetSpeed();
}

//--------------------------------------------------------------
// CFlcAnim::DisplayNextFrame
//--------------------------------------------------------------
BOOL									// FALSE if finished
CFlcAnim::DisplayNextFrame()
{
	ASSERT_VALID( this );
	ASSERT( IsInitialized() );

	if ( !GetFrame() )
		return FALSE;

	int	iChunkCount = m_ptrflcframe->GetHeader()->GetChunkCount();

	for ( int i = 0; i < iChunkCount; ++i )
		DoChunk();

	ASSERT_VALID( this );

	return TRUE;
}

//--------------------------------------------------------------
// CFlcAnim::DoChunk
//--------------------------------------------------------------
void
CFlcAnim::DoChunk()
{
	ASSERT_VALID( this );

	GetChunk();

	switch ( m_pchunkhdr->GetType() )
	{
		case CFlcChunkHeader::COLOR256_CHUNK:	DoColorChunk( TRUE  );	break;
		case CFlcChunkHeader::COLOR_CHUNK:		DoColorChunk( FALSE );	break;
		case CFlcChunkHeader::BRUN_CHUNK:		DoBRunChunk();				break;
		case CFlcChunkHeader::SS2_CHUNK:			DoSS2Chunk();				break;
		case CFlcChunkHeader::LC_CHUNK:			DoLCChunk();				break;
		case CFlcChunkHeader::BLACK_CHUNK:		DoBlackChunk();			break;
		case CFlcChunkHeader::COPY_CHUNK:		DoCopyChunk();				break;

		case CFlcChunkHeader::PSTAMP_CHUNK:		
		case CFlcChunkHeader::PREFIX_CHUNK:		
		default:	
		
			ASSERT( 0 );
			ThrowError( ERR_FLC_CHUNK_TYPE );
	}

	ASSERT_VALID( this );
}

//--------------------------------------------------------------
// CFlcAnim::DoColorChunk
//--------------------------------------------------------------
void
CFlcAnim::DoColorChunk(
	BOOL b256 )
{
	ASSERT_VALID( this );

	ASSERT( m_pby < m_ptrflcframe->GetBufferEnd() );

	WORD			nPackets = *( WORD * )m_pby;
	unsigned		uIndex   = 0;
	unsigned		nColors;

	m_pby += 2;

	while ( nPackets-- )
	{
		ASSERT( m_pby < m_ptrflcframe->GetBufferEnd() );

		uIndex += *m_pby++;

		ASSERT( m_pby < m_ptrflcframe->GetBufferEnd() );

		nColors = *m_pby++;

		if ( 0 == nColors )
			nColors = 256;

		for ( int i = uIndex; i < int( uIndex + nColors ); ++i )
		{
			ASSERT( m_pby + 3 < m_ptrflcframe->GetBufferEnd() );

			// Good old windoze BGR
		
			if ( b256 )
			{
				m_prgbquad[ i ].rgbRed   = *m_pby++;
				m_prgbquad[ i ].rgbGreen = *m_pby++;
				m_prgbquad[ i ].rgbBlue  = *m_pby++;
			}
			else
			{
				m_prgbquad[ i ].rgbRed   = ( *m_pby++ ) << 2;
				m_prgbquad[ i ].rgbGreen = ( *m_pby++ ) << 2;
				m_prgbquad[ i ].rgbBlue  = ( *m_pby++ ) << 2;
			}
		}

		if ( !IsIgnorePalette() )
			{
			thePal.SetColors( m_prgbquad, uIndex, nColors );
			m_pdib->SyncPalette ();
			}
	}

	if ( !IsIgnorePalette() )
		m_pdibwnd->Invalidate();

	ASSERT_VALID( this );
}

//--------------------------------------------------------------
// CFlcAnim::DoSS2Chunk
//--------------------------------------------------------------
void
CFlcAnim::DoSS2Chunk()
{
	ASSERT_VALID( this );

	WORD	wLines   = *( WORD * )m_pby;

	ASSERT( wLines <= m_flcheader.m_iHeight );

	int		nPackets;
	int		nWords;
	int		nBytes;
	unsigned	iCol;
	short		iVal;
	int		iDirPitch = m_pdib->GetDirPitch();

	CDIBits		 dibbits 	= m_pdib->GetBits();
	BYTE	 	  * pbyLineDst = dibbits + m_pdib->GetOffset( 0, 0 );
	BYTE	 	  * pbyDst     = NULL;
	BYTE const * pby 			= NULL;

	CRect			 rect( 10000, 0, 0, 0 );

	m_pby += 2;

	ASSERT_STRICT( m_pby < m_ptrflcframe->GetBufferEnd() );

	while ( wLines-- )
	{
		nPackets = 0;

		ASSERT_STRICT( m_pby < m_ptrflcframe->GetBufferEnd() );

		for ( ;; )
		{
			iVal   = *( short * )m_pby;
			m_pby += 2;

			if ( !( iVal & 0x8000 ))	// # packets
			{
				ASSERT_STRICT( !( iVal & 0x4000 ));

				nPackets = iVal;

				break;
			}
			else if ( iVal & 0x4000 )	// Skip rows
			{
				if ( rect.left <= rect.right )
					m_pdibwnd->Invalidate( &rect );

				pbyLineDst 	+= abs( iVal ) * iDirPitch;

				int	iTop = rect.bottom + abs( iVal );

				rect = CRect( 10000, iTop, 0, iTop );

				ASSERT_STRICT( 0 <= rect.top && rect.top < m_flcheader.m_iHeight );
			}
			else	// Last pixel on this row
			{
				// TRAP();
				// ASSERT( 1 == m_flcheader.m_iWidth % 2 );

				// FIXIT: implement (for odd-width flcs)
				// Just ignore for now
			}
		}

		rect.bottom++;

		pbyDst = pbyLineDst;

		ASSERT_STRICT( rect.bottom <= m_flcheader.m_iHeight );

		iCol = 0;

		if ( nPackets )
			rect.left = Min( rect.left, ( long )*m_pby );

		while ( nPackets-- )
		{
			ASSERT_STRICT( m_pby < m_ptrflcframe->GetBufferEnd() );

			int iColSkip = ( int )*m_pby++;

			ASSERT_STRICT( 0 <= iColSkip );

			iCol   += iColSkip;
			pbyDst += iColSkip;	// Column skip count

			ASSERT_STRICT( m_pby < m_ptrflcframe->GetBufferEnd() );

			nWords = *( signed char * )m_pby;

			ASSERT_STRICT( 0 <= nWords || *m_pby > 127 );
			ASSERT_STRICT( nWords < 0  || *m_pby < 128 );

			m_pby++;

			if ( 0 < nWords )
			{
				nBytes = nWords << 1;
				pby    = m_pby;
				iCol  += nBytes;

				ASSERT_STRICT( iCol <= ( unsigned )m_flcheader.m_iWidth );
				ASSERT_STRICT( GetDIB()->IsInRange( pbyDst, nBytes ));
  				ASSERT_STRICT( m_pby + nBytes <= m_ptrflcframe->GetBufferEnd() );

				memcpy( pbyDst, pby, nWords << 1 );

//				_asm	mov	edi, [pbyDst]
//				_asm	mov	esi, [pby]
//				_asm	mov	ecx, [nWords]
//				_asm	rep	movsw

				m_pby  += nBytes;
				pbyDst += nBytes;
			}
			else if ( nWords < 0 )
			{
				nWords = -nWords;
				nBytes =  nWords << 1;
				iCol  +=  nBytes;

				ASSERT_STRICT( iCol <= ( unsigned )m_flcheader.m_iWidth );
				ASSERT_STRICT( GetDIB()->IsInRange( pbyDst, nBytes ));
  				ASSERT_STRICT( m_pby < m_ptrflcframe->GetBufferEnd() );

				DWORD dwVal = *( DWORD * )m_pby;

				_asm	mov	edi, 	[pbyDst]
				_asm	mov	eax, 	[dwVal]
				_asm	mov	ecx, 	[nWords]
				_asm	rep	stosw

				m_pby  += 2;
				pbyDst += nBytes;
			}
		}

		rect.right  = Max( rect.right, ( long )iCol );
		pbyLineDst += iDirPitch;
	}

	m_pdibwnd->Invalidate( &rect );

	ASSERT_VALID( this );
}

//--------------------------------------------------------------
// CFlcAnim::DoLCChunk
//--------------------------------------------------------------
void
CFlcAnim::DoLCChunk()
{
	ASSERT_VALID( this );

	ASSERT( 0 );	// May be obsolete

	ASSERT_VALID( this );
}

//--------------------------------------------------------------
// CFlcAnim::DoBlackChunk
//--------------------------------------------------------------
void
CFlcAnim::DoBlackChunk()
{
	ASSERT_VALID( this );

	m_pdib->Clear();

	m_pdibwnd->Invalidate();

	ASSERT_VALID( this );
}

//--------------------------------------------------------------
// CFlcAnim::DoBRunChunk
//--------------------------------------------------------------
void
CFlcAnim::DoBRunChunk()
{
	ASSERT_VALID( this );

	CDIBits	dibbits 		= m_pdib->GetBits();
	BYTE	 * pbyLineDst 	= dibbits + m_pdib->GetOffset( 0, 0 );
	BYTE	 * pbyDst 		= NULL;
	int		iType  		= 0;
	int		iHeight 		= GetHeight();
	int		iPitch		= m_pdib->GetDirPitch();
	int		iBytesWritten;
	int		iBytes;

	for ( ; iHeight--; pbyLineDst += iPitch )
	{
		pbyDst 		  = pbyLineDst;
		iBytesWritten = 0;

		for ( m_pby++; iBytesWritten < GetWidth(); pbyDst += iBytes )
		{
			ASSERT_STRICT( m_pby < m_ptrflcframe->GetBufferEnd() );

			iBytes = *( signed char * )m_pby;

			ASSERT_STRICT( iBytes >= 0 || *m_pby > 127 );
			ASSERT_STRICT( iBytes  < 0 || *m_pby < 128 );

			++m_pby;

			if ( 0 < iBytes )
			{
				iBytesWritten += iBytes;
				ASSERT_STRICT( iBytesWritten <= GetWidth() );
  				ASSERT_STRICT( m_pby < m_ptrflcframe->GetBufferEnd() );
				memset( pbyDst, *m_pby, iBytes );

				m_pby++;
			}
			else
			{
				iBytes = -iBytes;

				ASSERT_STRICT( 0 < iBytes );

				iBytesWritten += iBytes;

				ASSERT_STRICT( iBytesWritten <= GetWidth() );
				ASSERT_STRICT( m_pby + iBytes <= m_ptrflcframe->GetBufferEnd() );
				memcpy( pbyDst, m_pby, iBytes );

				m_pby += iBytes;
			}
		}

		ASSERT_STRICT( GetWidth() == iBytesWritten );
	}

	m_pdibwnd->Invalidate();

	ASSERT_VALID( this );
}

//--------------------------------------------------------------
// CFlcAnim::DoCopyChunk
//--------------------------------------------------------------
void
CFlcAnim::DoCopyChunk()
{
	ASSERT_VALID( this );

	ASSERT( m_pby + m_flcheader.m_iHeight * m_flcheader.m_iWidth <= m_ptrflcframe->GetBufferEnd() );

	m_pdib->SetBits( m_pby );

	m_pby += GetHeight() * GetWidth();

	m_pdibwnd->Invalidate();

	ASSERT_VALID( this );
}

//--------------------------------------------------------------
// CFlcAnim::GetChunk
//--------------------------------------------------------------
void
CFlcAnim::GetChunk()
{
	ASSERT_VALID( this );

	m_pchunkhdr = ( CFlcChunkHeader * )m_pby;

	ASSERT( m_pchunkhdr->AssertValid() );

	m_pby += sizeof( CFlcChunkHeader );

	ASSERT_VALID( this );
}

//--------------------------------------------------------------
// CFlcAnim::GetFrame
//--------------------------------------------------------------
BOOL
CFlcAnim::GetFrame()
{
	ASSERT_VALID( this );

	if ( m_ptrflcframe->GetFrame() >= m_flcheader.GetFrameCount() - 1 )
		return FALSE;

	m_ptrflcframe->LoadNextFrame();

	m_pby = m_ptrflcframe->GetBuffer();

	ASSERT_VALID( this );

	return TRUE;
}

//--------------------------------------------------------------
// CFlcAnim::AssertValid
//--------------------------------------------------------------
#ifdef _DEBUG
void
CFlcAnim::AssertValid() const
{
	CObject::AssertValid();

	m_flcheader.AssertValid();

	if ( !m_bInitialized )
		return;

	ASSERT_VALID( m_pdibwnd );
	ASSERT_VALID( &m_colorformat );

	// FIXIT: etc., etc.
}
#endif

//======== Autodesk Animator Pro Flic Files (FLC) ======== 
/*
This is the main animation file format created by Autodesk Animator Pro. The 
file contains a 128-byte header, followed by an optional prefix chunk, 
followed by one or more frame chunks. 

The prefix chunk, if present, contains Autodesk Animator Pro settings 
information, cel placement information, and other auxiliary data.

A frame chunk exists for each frame in the animation. In addition, a ring 
frame follows all the animation frames. Each frame chunk contains color 
palette information and/or pixel data.

The ring frame contains delta-compressed information to loop from the last 
frame of the flic back to the first. It can be helpful to think of the ring 
frame as a copy of the first frame, compressed in a different way. All flic 
files contain a ring frame, including a single-frame flic.


The FLC File Header

A .flc file begins with a 128-byte header, described below. All lengths and 
offsets are in bytes. All values stored in the header fields are unsigned.

Offset  Length  Name            Description
0       4       size            The size of the entire animation file, 
				including this file header.
4       2       magic           File format identifier. Always hex AF12.
6       2       frames          Number of frames in the flic. This count does 
				not include the ring frame. Flic files have a 
				maximum length of 4000 frames.
8       2       width           Screen width in pixels.
10      2       height          Screen height in pixels.
12      2       depth           Bits per pixel (always 8).
14      2       flags           Set to hex 0003 after ring frame is written 
				and flic header is updated. This indicates 
				that the file was properly finished and 
				closed.
16      4       speed           Number of milliseconds to delay between each 
				frame during playback.
20      2       reserved        Unused word, set to 0.
22      4       created         The MSDOS-formatted date and time of the 
				file's creation.
26      4       creator         The serial number of the Autodesk Animator 
				Pro program used to create the file. If the 
				file was created by some other program using 
				FlicLib, this value is hex 464C4942 ("FLIB").
30      4       updated         The MSDOS-formatted date and time of the 
				file's most recent update.
34      4       updater         Indicates who last updated the file. See the 
				description of creator.
38      2       aspectx         The x-axis aspect ratio at which the file was 
				created.
40      2       aspecty         The y-axis aspect ratio at which the file was 
				created. Most often, the x:y aspect ratio is 
				1:1. A 320x200 flic has a ratio of 6:5.
42      38      reserved        Unused space, set to zeroes.
80      4       oframe1         Offset from the beginning of the file to the 
				first animation frame chunk.
84      4       oframe2         Offset from the beginning of the file to the 
				second animation frame chunk. This value is 
				used when looping from the ring frame back to 
				the second frame during playback.
88      40      reserved        Unused space, set to zeroes.


The FLC Prefix Chunk

An optional prefix chunk can immediately follow the animation file header. 
This chunk is used to store auxiliary data that is not directly involved in 
the animation playback. The prefix chunk starts with a 6-byte header, as 
follows:

Offset  Length  Name            Description
0       4       size            The size of the prefix chunk, including this 
				header and all subordinate chunks that 
				follow.
4       2       type            Prefix chunk identifier. Always hex F100.

Note: Programs other than Autodesk Animator Pro should never need to create 
flic files that contain a prefix chunk. Programs reading a flic file should 
skip the prefix chunk by using the size value in the prefix header to read 
and discard the prefix, or by seeking directly to the first frame using the 
oframe1 field from the file header.


The FLC Frame Chunks

Frame chunks contain the pixel and color data for the animation. A frame 
chunk can contain multiple subordinate chunks, each containing a different 
type of data for the current frame. Each frame chunk starts with a 16-byte 
header that describes the contents of the frame, as follows:

Offset  Length  Name            Description
0       4       size            The size of the frame chunk, including this 
				header and all subordinate chunks that 
				follow.
4       2       type            Frame chunk identifier. Always hex F1FA.
6       2       chunks          Number of subordinate chunks in the frame 
				chunk.
8       8       reserved        Unused space, set to zeroes.


Immediately following the frame header are the frame's subordinate data 
chunks. When the chunks count in the frame header is zero, it indicates that 
this frame is identical to the previous frame. This implies that no change is 
made to the screen or color palette, but the appropriate delay is still 
inserted during playback.


Each data chunk within a frame chunk is formatted as follows:

Offset  Length  Name    Description
0       4       size    The size of the chunk, including this header.
4       2       type    Data type identifier.
6     (size-6)  data    The color or pixel data.


The type values in the chunk headers indicate what type of graphics data the 
chunk contains and which compression method was used to encode the data. The 
following values (and their associated mnemonic names) are currently found in 
frame data chunks:

Value   Name            Description
4       FLI_COLOR256    256-level color palette info
7       FLI_SS2         Word-oriented delta compression
11      FLI_COLOR       64-level color palette info
12      FLI_LC          Byte-oriented delta compression
13      FLI_BLACK       Entire frame is color index 0
15      FLI_BRUN        Byte run length compression
16      FLI_COPY        No compression
18      FLI_PSTAMP      Postage stamp sized image


The following sections describe each of these data encoding methods in 
detail.

Chunk Type 4 (FLI_COLOR256): 256-Level Color

The data in this chunk is organized in packets. The first word following the 
chunk header is a count of the number of packets in the chunk. Each packet 
consists of a one-byte color index skip count, a one-byte color count, and 
three bytes of color information for each color defined.

At the start of the chunk, the color index is assumed to be zero. Before 
processing any colors in a packet, the color index skip count is added to 
the current color index. The number of colors defined in the packet is 
retrieved. A zero in this byte indicates 256 colors follow. The three bytes 
for each color define the red, green, and blue components of the color in 
that order. Each component can range from 0 (off) to 255 (full on). 

The data to change colors 2,7,8, and 9 would appear as follows:

2                                  ; two packets 
2,1,r,g,b                          ; skip 2, change 1 
4,3,r,g,b,r,g,b,r,g,b              ; skip 4, change 3


Chunk Type 11 (FLI_COLOR): 64-Level Color

This chunk is identical to FLI_COLOR256 except that the values for the red, 
green, and blue components are in the range of 0-63 instead of 0-255.


Chunk Type 13 (FLI_BLACK): No Data

This chunk has no data following the header. All pixels in the frame are set 
to color index 0.


Chunk Type 16 (FLI_COPY): No Compression

This chunk contains an uncompressed image of the frame. The number of pixels 
following the chunk header is exactly the width of the animation times the 
height of the animation. The data starts in the upper left corner with pixels 
copied from left to right and then top to bottom. This type of chunk is 
created when the preferred compression method (SS2 or BRUN) generates more 
data than the uncompressed frame image�a relatively rare situation.


Chunk Type 15 (FLI_BRUN): Byte Run Length Compression

This chunk contains the entire image in a compressed format. Usually this 
chunk is used in the first frame of an animation, or within a postage stamp 
image chunk. 

The data is organized in lines. Each line contains packets of compressed 
pixels. The first line is at the top of the animation, followed by 
subsequent lines moving downward. The number of lines in this chunk is given 
by the height of the animation.

The first byte of each line is a count of packets in the line. This value is 
ignored; it is a holdover from Autodesk Animator. It is possible to generate 
more than 255 packets on a line. The width of the animation is now used to 
drive the decoding of packets on a line; continue reading and processing 
packets until width pixels have been processed, then proceed to the next 
line.

Each packet consists of a type/size byte, followed by one or more pixels. If 
the packet type is negative, it is a count of pixels to be copied from the 
packet to the animation image. If the packet type is positive, it contains a 
single pixel that is to be replicated; the absolute value of the packet type 
is the number of times the pixel is to be replicated.


Chunk Type 12 (FLI_LC): Byte Aligned Delta Compression

This chunk contains the differences between the previous frame and this 
frame. This compression method was used by Autodesk Animator, but is not 
created by Autodesk Animator Pro. This type of chunk can appear in an 
Autodesk Animator Pro file; however, if the file was originally created by 
Autodesk Animator, then some (but not all) frames were modified using 
Autodesk Animator Pro. 

The first 16-bit word following the chunk header contains the position of the 
first line in the chunk. This is a count of lines (down from the top of the 
image) that are unchanged from the prior frame. The second 16-bit word 
contains the number of lines in the chunk. The data for the lines follows 
these two words.

Each line begins with two bytes. The first byte contains the starting x 
position of the data on the line, and the second byte contains the number of 
packets for the line. Unlike BRUN compression, the packet count is 
significant (because this compression method is only used on 320x200 flics).

Each packet consists of a single byte column skip, followed by a packet 
type/size byte. If the packet type is positive, it is a count of pixels to be 
copied from the packet to the animation image. If the packet type is 
negative, it contains a single pixel which is to be replicated; the absolute 
value of the packet type gives the number of times the pixel is to be 
replicated.

Note: The negative/positive meaning of the packet type bytes in LC 
compression is reversed from that used in BRUN compression. This gives better 
performance during playback.


Chunk Type 7 (FLI_SS2): Word Aligned Delta Compression

This format contains the differences between consecutive frames. This is the 
format most often used by Autodesk Animator Pro for frames other than the 
first frame of an animation. It is similar to the line coded delta (LC) 
compression, but is word oriented instead of byte oriented. The data is 
organized into lines and each line is organized into packets.

The first word in the data following the chunk header contains the number of 
lines in the chunk. Each line can begin with some optional words that are 
used to skip lines and set the last byte in the line for animations with odd 
widths. These optional words are followed by a count of the packets in the 
line. The line count does not include skipped lines.

The high order two bits of the word is used to determine the contents of the 
word.

Bit 15  Bit 14  Meaning
0       0       The word contains the packet count. The packets follow this 
		word. The packet count can be zero; this occurs when only the 
		last pixel on a line changes.
1       0       The low order byte is to be stored in the last byte of the 
		current line. The packet count always follows this word.
1       1       The word contains a line skip count. The number of lines 
		skipped is given by the absolute value of the word. This word 
		can be followed by more skip counts, by a last byte word, or 
		by the packet count.

The packets in each line are similar to the packets for the line coded chunk. 
The first byte of each packet is a column skip count. The second byte is a 
packet type. If the packet type is positive, the packet type is a count of 
words to be copied from the packet to the animation image. If the packet type 
is negative, the packet contains one more word that is to be replicated. The 
absolute value of the packet type gives the number of times the word is to be 
replicated. The high and low order byte in the replicated word do not 
necessarily have the same value.


Chunk Type 18 (FLI_PSTAMP): Postage Stamp Image

This chunk type holds a postage stamp�a reduced-size image�of the frame. It 
generally appears only in the first frame chunk within a flic file. 

When creating a postage stamp, Autodesk Animator Pro considers the ideal size 
to be 100x63 pixels. The actual size varies as needed to maintain the same 
aspect ratio as the original.

The pixels in a postage stamp image are mapped into a six-cube color space, 
regardless of the color palette settings for the full frame image. A six-cube 
color space is formed as follows:
	
	start at palette entry 0
	for red = 0 thru 5
		for green = 0 thru 5
			for blue = 0 thru 5
				palette_red   = (red   * 256)/6 
				palette_green = (green * 256)/6
				palette_blue  = (blue  * 256)/6
				move to next palette entry
			end for blue
		end for green
	end for red

Any arbitrary RGB value (where each component is in the range of 0-255) can 
be mapped into the six-cube space using this formula:

((6*red)/256)*36 + ((6*green)/256)*6 + ((6*blue)/256)


When a frame data chunk has been identified as a postage stamp, the header 
for the chunk contains more fields than just size and type. The full postage 
stamp chunk header is defined as follows:

Offset  Length  Name            Description
0       4       size            The size of the postage stamp chunk, 
				including this header.
4       2       type            Postage stamp identifier; always 18.
6       2       height          Height of the postage stamp image, in pixels.
8       2       width           Width of the postage stamp image, in pixels.
10      2       xlate           Color translation type; always 1, indicating 
				six-cube color space.


Immediately following this header is the postage stamp data. The data is 
formatted as a chunk with standard size/type header. The type will be one of 
the following:

Value   Name            Description
15      FPS_BRUN        Byte run length compression
16      FPS_COPY        No compression
18      FPS_XLAT256     Six-cube color xlate table

The FPS_BRUN and FPS_COPY types are identical to the FLI_BRUN and -FLI_COPY 
encoding methods described above.

The FPS_XLAT256 type indicates that the chunk contains a 256-byte color 
translation table instead of pixel data. To process this type of postage 
stamp, read the pixel data for the full-sized frame image, and translate its 
pixels into six-cube space using a lookup in the 256-byte color translation 
table. This type of postage stamp appears when the size of the animation 
frames is smaller than the standard 100x63 postage stamp size.

*/
